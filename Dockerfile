FROM node:18-alpine AS build

RUN mkdir /app
WORKDIR /app
ADD package.json /app/package.json
RUN npm install
COPY . .
RUN npm run build

###############################

FROM node:18-alpine
RUN mkdir /app
WORKDIR /app
ADD package.json /app/package.json
ADD tsconfig.json /app/tsconfig.json
ADD .env /app/.env
COPY --from=build /app/node_modules ./node_modules
COPY --from=build /app/dist ./dist
EXPOSE 3000
CMD ["node", "dist/index.js"]
