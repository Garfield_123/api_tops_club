# Awesome Project Build with TypeORM

Steps to run this project:

1. Run `npm i` command
2. Setup database settings inside `data-source.ts` file
3. Run `npm run dev` command for start project locally by using `nodemon`
4. Run `npm start` command for start project locally by not using `nodemon`

Steps to deploy docker

1. Run `npm run build-docker` command for build docker image
1. Run `npm run build-docker-amd` command for build docker image amd64 platform
2. Run `npm run start-docker` command for run docker image

Steps to create migration files

1. Run `FILENAME=Example npm run migrate:create` command for creating migration file

Steps to run migration files

1. Run `npm run migrate:up` command
