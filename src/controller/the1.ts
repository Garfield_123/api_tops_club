import { EApiLogSystem } from "../types/apiLog";
import { manager } from "../manager";
import { IAuthUser } from "../types/the1";

export const getAccessTokenByOAuth = async (req, res, next) => {
    try {
        await manager.the1.getAccessTokenByOAuth(req, res);
    } catch (e) {
        if (e.response && e.response.data) {
            const { status, data } = e.response;
            const { code, description, name } = data.error;
            const response = {
                code: status,
                data: {},
                message: `${name} (${code}): ${description}`
            }
            await manager.apiLog.saveLogError({
                apiRoute: req.originalUrl,
                system: EApiLogSystem.STS,
                request: JSON.stringify(req.query),
                response: JSON.stringify(response),
                remark: 'The1 Callback Failed',
            })
            return res.status(status).json(response);
        }
        next(e);
    }
}

export const requestOtp = async (req, res, next) => {
    try {
        await manager.the1.requestOtp(req, res);
    } catch (e) {
        next(e);
    }
}

export const loginOtp = async (req, res, next) => {
    try {
        await manager.the1.loginOtp(req, res);
    } catch (e) {
        next(e);
    }
}

export const refreshToken = async (req, res, next) => {
    try {
        const generated = await manager.the1.refreshToken(req, res);
    } catch (e) {
        next(e);
    }
}

export const updateCentralThe1Segment = async (req, res, next) => {
    try {
        const { cardNo, segmentCode }: { cardNo: string, segmentCode: string } = req.body;

        if (!cardNo || !segmentCode) {
            return res.status(400).json({
                code: 400,
                data: {},
                message: 'Cardno and Segment Code are required.'
            });
        }

        const result = await manager.the1.updateCentralThe1Segment(cardNo, segmentCode);
        if (result?.displayErrorMessage) {
            return res.status(400).json({
                code: 400,
                data: {},
                message: result?.displayErrorMessage
            });
        }

        return res.status(200).json({
            code: 200,
            data: {},
            message: 'Success.'
        });
    } catch (e) {
        next(e);
    }
}

export const deleteCentralThe1Segment = async (req, res, next) => {
    try {
        const { cardNo, segmentCode }: { cardNo: string, segmentCode: string } = req.body;

        if (!cardNo || !segmentCode) {
            return res.status(400).json({
                code: 400,
                data: {},
                message: 'Cardno and Segment Code are required.'
            });
        }
        
        const result = await manager.the1.deleteCentralThe1Segment(cardNo, segmentCode);
        if (result?.displayErrorMessage) {
            return res.status(400).json({
                code: 400,
                data: {},
                message: result?.displayErrorMessage
            });
        }

        return res.status(200).json({
            code: 200,
            data: {},
            message: 'Success.'
        });
    } catch (e) {
        next(e);
    }
}
    
export const signOut = async (req, res, next) => {
    try {
        const { customer_id: customerId }: IAuthUser = req.user;

        if (!customerId) {
            return res.status(400).json({
                code: 400,
                data: {},
                message: 'Customer ID is required.'
            });
        }
        
        const result = await manager.the1.signOut(customerId);
        if (!result.passed) {
            return res.status(400).json({
                code: 400,
                data: {},
                message: result.errorMessage
            });
        }
        return res.status(200).json({
            code: 200,
            data: {},
            message: 'Success.'
        });
    } catch (e) {
        next(e);
    }
}
