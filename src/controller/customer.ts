import { ENotiType } from "../types/customer";
import { manager } from "../manager";
import { IAuthUser } from "../types/the1";
import { DUMMY_CUSTOMER_ID } from "../utils/constants";

export const checkMember = async (req, res, next) => {
  try {
    const { mobile, segment, deviceId } = req.body;

    if (!mobile) {
      return res.status(400).json({
        code: 400,
        data: {},
        message: "กรุณาระบุหมายเลขโทรศัพท์",
      });
    }

    const result = await manager.customer.checkMember(mobile, segment);
    res.status(200).json({
      code: 200,
      data: {
        isMember: !!result,
        isNewDevice: deviceId && result?.last_device_id && deviceId !== result?.last_device_id
      },
      message: "success",
    });
  } catch (e) {
    next(e);
  }
};

export const changeMemberEmail = async (req, res, next) => {
  try {
    const { customer_id: customerId }: IAuthUser = req.user;
    const { email } = req.body;
    if (!customerId || !email) {
      return res.status(400).json({
        code: 400,
        message: "กรุณาใส่ค่า customerId และ email",
      });
    }
    const result = await manager.customer.changeCustomerEmail(
      customerId,
      email
    );
    return res.status(200).json({
      code: 200,
      message: "เปลี่ยน email สำเร็จ",
    });
  } catch (error) {}
};

export const register = async (req, res, next) => {
  try {
    const { customer_id: customerId }: IAuthUser = req.user;
    const { isTrial } = req.body;

    const result = await manager.customer.register(customerId, isTrial);
    res.status(200).json({
      code: 200,
      data: result,
      message: "success",
    });
  } catch (e) {
    next(e);
  }
};

export const saveProductFavorite = async (req, res, next) => {
  try {
    const { customer_id: customerId }: IAuthUser = req.user;
    const {
      prCode,
      isFavorite,
      qty,
    }: {
      prCode: string;
      isFavorite: boolean;
      qty: number;
    } = req.body;

    if (!customerId) {
      return res.status(400).json({
        code: 400,
        data: {},
        message: "Customer ID is required.",
      });
    }

    if (!prCode) {
      return res.status(400).json({
        code: 400,
        data: {},
        message: "Product is not exists.",
      });
    }

    const customer = await manager.customer.findByCustomerId(customerId);
    if (!customer) {
      return res.status(400).json({
        code: 400,
        data: {},
        message: "Customer is not exists.",
      });
    }

    await manager.customer.saveProductFavorite(
      customer,
      prCode,
      isFavorite,
      qty
    );
    res.status(200).json({
      code: 200,
      data: {},
      message: "success",
    });
  } catch (e) {
    next(e);
  }
};

export const getProductFavorite = async (req, res, next) => {
  try {
    const { customer_id: customerId }: IAuthUser = req.user;

    if (!customerId) {
      return res.status(200).json({
        code: 200,
        data: [],
        message: "success",
      });
    }

    const customer = await manager.customer.findByCustomerId(customerId);
    if (!customer) {
      return res.status(200).json({
        code: 200,
        data: [],
        message: "success",
      });
    }

    const customerSegment = await manager.customer.findSegment(
      customer?.segment_code
    );
    const data = await manager.customer.getProductFavorite(
      customer,
      customerSegment
    );
    res.status(200).json({
      code: 200,
      data: data,
      message: "success",
    });
  } catch (e) {
    next(e);
  }
};

export const getOrderHistory = async (req, res, next) => {
  try {
    const { customer_id: customerId }: IAuthUser = req.user;

    if (!customerId) {
      return res.status(400).json({
        code: 400,
        data: {},
        message: "Customer ID is required.",
      });
    }

    const customer = await manager.customer.findByCustomerId(customerId);
    if (!customer) {
      return res.status(400).json({
        code: 400,
        data: {},
        message: "Customer does not exist.",
      });
    }

    // const data = await manager.customer.getOrderHistory(customer);
    res.status(200).json({
      code: 200,
      data: {},
      message: "success",
    });
  } catch (e) {
    next(e);
  }
};

export const signOut = async (req, res, next) => {
  try {
    const { customer_id: customerId }: IAuthUser = req.user;

    await manager.customer.clearTokenThe1(customerId);
    await manager.customer.updateLogonLog(customerId, false);
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
};

export const getMembershipInfo = async (req, res, next) => {
  try {
    const { customer_id: customerId }: IAuthUser = req.user;
    const { device_id: deviceId }: {
      device_id: string;
    } = req.body;

    if (!customerId) {
      return res.status(200).json({
        code: 200,
        data: {},
        message: "Customer does not exists.",
      });
    }

    const customer = await manager.customer.findByCustomerId(customerId);
    if (!customer) {
      global.io.emit('auto-signout', {
        customerId: null,
        deviceId: deviceId
      });

      return res.status(400).json({
        code: 400,
        data: {},
        message: "Customer does not exists.",
      });
    }

    const data = customerId !== DUMMY_CUSTOMER_ID 
      ? await manager.customer.getMembershipInfo(customer) 
      : {
        isShowRenewalNotice: false,
      };
    res.status(200).json({
      code: 200,
      data: data,
      message: "success",
    });
  } catch (e) {
    next(e);
  }
};

export const saveNotification = async (req, res, next) => {
  try {
    const {
      customerId,
      notiType,
      isNoti,
    }: {
      customerId: number;
      notiType: string;
      isNoti: boolean;
    } = req.body;

    if (!ENotiType[notiType.toUpperCase()]) {
      return res.status(400).json({
        code: 400,
        data: {},
        message: "Notification type is not valid.",
      });
    }

    await manager.customer.saveNotification(
      customerId,
      ENotiType[notiType.toUpperCase()],
      isNoti
    );
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
};

export const removeCustomer = async (req, res, next) => {
  try {
    const { customer_id: customerId }: IAuthUser = req.user;
    const { fromRegister, isForceDelete, reasons, isCancel }: {
      isCancel: boolean
      fromRegister: boolean;
      isForceDelete: boolean;
      reasons?: {
        id: number;
        remark?: string;
      }[]
    } = req.body;

    if (!customerId) {
      return res.status(400).json({
        code: 400,
        data: {},
        message: "Required field is invalid.",
      });
    }

    if (!isCancel && !(reasons && reasons.length > 0)) {
      return res.status(400).json({
        code: 400,
        data: {},
        message: "Required field is invalid.",
      });
    }

    await manager.customer.removeCustomer(customerId, isCancel, fromRegister, isForceDelete, reasons);

    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
};

export const getMultiAddress = async (req, res, next) => {
  try {
    const { customer_id: customerId }: IAuthUser = req.user;

    if (!customerId) {
      return res.status(400).json({
        code: 400,
        data: [],
        message: "Customer does not exists.",
      });
    }

    const data = await manager.customer.getMultiAddress(customerId);
    res.status(200).json({
      code: 200,
      data: data,
      message: "success",
    });
  } catch (e) {
    next(e);
  }
};

export const addMultiAddress = async (req, res, next) => {
  try {
    const { customer_id: customerId }: IAuthUser = req.user;

    if (!customerId) {
      return res.status(200).json({
        code: 200,
        data: {},
        message: "Customer does not exists.",
      });
    }

    await manager.customer.addMultiAddress(customerId, req.body);
    res.status(200).json({
      code: 200,
      data: {},
      message: "success",
    });
  } catch (e) {
    next(e);
  }
};

export const deleteMultiAdress = async (req, res, next) => {
  try {
    const { customer_id: customerId }: IAuthUser = req.user;

    if (!customerId) {
      return res.status(200).json({
        code: 200,
        data: {},
        message: "Customer does not exists.",
      });
    }

    await manager.customer.deleteMultiAdress(customerId, req.body);
    res.status(200).json({
      code: 200,
      data: {},
      message: "success",
    });
  } catch (e) {
    next(e);
  }
};

export const getBenefit = async (req, res, next) => {
  try {
    const { type } = req.body;

    if (type && !['guest', 'member', 'trial'].includes(type)) {
      return res.status(400).json({
        code: 400,
        data: {},
        message: "Benefit type is invalid.",
      });
    }
    
    const result = await manager.customer.getBenefit(type)
    res.status(200).json({
      code: 200,
      data: result,
      message: "success",
    });
  } catch (e) {
    next(e);
  }
}

export const getTermsAndConditions = async (req, res, next) => {
  try {
    const { type } = req.body;

    if (!['member', 'privacy'].includes(type)) {
      return res.status(404).json({
        code: 200,
        data: {},
        message: "Required parameters is invalid.",
      })
    }

    const result = await manager.customer.getTermsAndConditions(type)
    res.status(200).json({
      code: 200,
      data: result,
      message: "success",
    });
  } catch (e) {
    next(e);
  }
}

export const updateProfileImage = async (req, res, next) => {
  try {
    const { files } = req;
    const { customer_id: customerId }: IAuthUser = req.user;

    await manager.customer.updateProfileImage(customerId, files);
    res.status(200).json({
      code: 200,
      data: {},
      message: "success",
    });
  } catch (e) {
    next(e);
  }
};
