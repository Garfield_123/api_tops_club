import { manager } from "../manager";

export const getDetail = async (req, res, next) => {
  try {
    const data = await manager.product.getDetail(req, res);

    res.status(200).json({
      message: "success",
      code: 200,
      data: data,
    });
  } catch (e) {
    next(e);
  }
};
export const agoliaSearch = async (req, res, next) => {
  try {
    let limit = 999;
    if (req.body.page == 1) {
      limit = 3;
    }
    const customer = req.body.cus_id
      ? await manager.customer.findByCustomerId(req.body.cus_id)
      : null;
    const customerSegment = await manager.customer.findSegment(
      customer?.segment_code
    );
    let result = await manager.product.algoliaSearch(
      req.body.query,
      req.body.store_id,
      999,
      1
    );

    let get_content = [];
    if (result.skuArray && result.skuArray.length > 0) {
      get_content = await manager.shopBy.getALlProductDetails(
        customer,
        customerSegment,
        result.skuArray
      );
    }
    res.status(200).send({
      code: 200,
      message: "Success",
      data: {
        products: get_content,
      },
    });
    return;
  } catch (e) {
    next(e);
  }
};
