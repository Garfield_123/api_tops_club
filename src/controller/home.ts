import { manager } from "../manager";

export const getHomePage = async (req, res, next) => {
    try {
        const { screen_id: screenId, customerId, lang }: {
            screen_id: number;
            customerId: number;
            lang: string;
        } = req.body;

        if (!screenId) {
            return res.status(400).json({
                message: "Screen ID is required.",
                code: 400,
                data: {},
            });
        }
        const data = await manager.home.getPageDetail(screenId, customerId, lang);
        res.status(200).json({
            message: "success",
            code: 200,
            data: data,
        });
    } catch (e) {
      next(e);
    }
};