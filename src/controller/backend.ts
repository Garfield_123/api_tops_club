import { manager } from "../manager";

export const couponLists = async (req, res, next) => {
    try {
      const data = await manager.backend.couponLists(req);
      res.status(200).json({
        message: "success",
        code: 200,
        data: data,
      });
    } catch (e) {
      next(e);
    }
};