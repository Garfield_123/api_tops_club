import { ELanguage } from "../types/shopBy";
import { manager } from "../manager";

export const getShopBy = async (req, res, next) => {
    try {
        const { lang, branchId } = req.query;
        const data = await manager.shopBy.getPageDetail(lang || ELanguage.EN, branchId)
        
        res.status(200).json({
            message: "success",
            code: 200,
            data: data
        });
    } catch (e) {
        next(e);
    }
}

export const getShopByPLP = async (req, res, next) => {
    try {
        const data = await manager.shopBy.getPLPDetail(req, res)
        
        res.status(200).json({
            message: "success",
            code: 200,
            data: data
        });
    } catch (e) {
        next(e);
    }
}

export const getFilterLists = async (req, res, next) => {
    try {
        const { lang, branchId, type, code, isSimilar } = req.query;
        const data = await manager.shopBy.getFilterLists(lang || ELanguage.EN, type || null, code || null, branchId, isSimilar)
        
        res.status(200).json({
            message: "success",
            code: 200,
            data: data
        });
    } catch (e) {
        next(e);
    }
}
