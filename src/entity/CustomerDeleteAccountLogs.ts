import { Entity, Column, BaseEntity, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn } from "typeorm"

@Entity("customer_delete_account_logs")
export class CustomerDeleteAccountLogs extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number
    @Column()
    customer_id: number
    @Column()
    reason_id: number
    @Column()
    remark: string
    @CreateDateColumn()
    created_at: Date
    @UpdateDateColumn()
    updated_at: Date
}
