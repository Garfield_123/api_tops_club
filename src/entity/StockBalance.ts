import { Entity, Column, BaseEntity, UpdateDateColumn, PrimaryGeneratedColumn, CreateDateColumn, OneToMany, JoinColumn } from "typeorm"
import { MappingProducts } from "./MappingProducts"

@Entity("stock_balance")
export class StockBalance extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number
    @Column()
    st_code: string
    @Column()
    pr_code: string
    @Column()
    stock_in: string
    @Column()
    stock_out: string
    @CreateDateColumn()
    created_at: Date
    @UpdateDateColumn()
    updated_at: Date

    @OneToMany((_type) => MappingProducts, (item) => item.stockBalance)
    @JoinColumn({ name: 'pr_code', referencedColumnName: 'pr_code' })
    products: MappingProducts[]
}
