import { Entity, Column, BaseEntity, CreateDateColumn, UpdateDateColumn, PrimaryColumn } from "typeorm"

@Entity("product_discount")
export class ProductDiscount extends BaseEntity {
    @PrimaryColumn()
    pr_code: string
    @Column()
    percent_discount: number
    @CreateDateColumn()
    created_at: Date
    @Column()
    created_by: string
    @UpdateDateColumn()
    updated_at: Date
    @Column()
    updated_by: string
}
