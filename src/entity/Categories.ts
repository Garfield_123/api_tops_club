import {
  Entity,
  Column,
  BaseEntity,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  OneToMany,
  JoinColumn,
} from "typeorm";
import { MappingProducts } from "./MappingProducts";

@Entity("categories")
export class Categories extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  category_root_id: number;
  @Column()
  category_code: string;
  @Column()
  category_code_user: string;
  @Column()
  category_name_th: string;
  @Column()
  category_name_en: string;
  @Column()
  category_image_th: string;
  @Column()
  category_image_en: string;
  @Column()
  category_icon_th: string;
  @Column()
  category_icon_en: string;
  @Column()
  cms_key: string;
  @Column()
  is_active: boolean;
  @CreateDateColumn()
  created_at: Date;
  @Column()
  created_by: string;
  @UpdateDateColumn()
  updated_at: Date;
  @Column()
  updated_by: string;
  @Column()
  category_stock_minimum: number;
  @Column()
  unit: string;
  @Column()
  category_icon: string;
  @Column()
  percent_discount: number;
  @Column()
  sequence: number;

  @OneToMany((_type) => MappingProducts, (item) => item.categoryProduct)
  @JoinColumn({ name: "category_code", referencedColumnName: "cat_code" })
  products: MappingProducts[];
}
