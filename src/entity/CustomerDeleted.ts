import { Entity, Column, BaseEntity, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn, OneToMany, JoinColumn } from "typeorm"
import { ProductFavorite } from "./ProductFavorite"

@Entity("customer_deleted")
export class CustomerDeleted extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number
    @Column()
    customer_id: number
    @Column()
    first_name: string
    @Column()
    last_name: string
    @Column()
    t1_card_no: string
    @Column()
    t1_points_balance: number
    @Column()
    email: string
    @Column()
    mobile: string
    @Column()
    billing_address: string
    @Column()
    shipping_address: string
    @Column()
    is_active: boolean
    @Column()
    customer_image: string
    @Column()
    membership_status: boolean
    @Column()
    membership_enrolled_date: Date
    @Column()
    membership_expired_date: Date
    @CreateDateColumn()
    created_at: Date
    @Column()
    segment_code: string
    @Column()
    first_name_en: string
    @Column()
    last_name_en: string
    @Column()
    is_staff: string
    @Column()
    multiaddress: string
    @Column()
    is_trial: boolean
    @Column()
    employee_id: string
    @Column()
    personal_id: string
    @Column()
    payment_date: Date
    @Column()
    t1_access_token: string
    @Column()
    t1_refresh_token: string
    @Column()
    membership_register_date: Date
    @Column()
    is_membership_cancelled: boolean
    @Column()
    t1_user_account_id: string
    @Column()
    is_member_renewing: boolean
    @Column()
    is_notification: boolean
    @Column()
    is_email: boolean
    @Column()
    is_sms: boolean
    @Column()
    last_device_id: string
    @Column()
    last_device_logon_date: Date
    @Column()
    is_delete_account: boolean
    @Column()
    language: string
    @Column()
    request_delete_at: Date
}
