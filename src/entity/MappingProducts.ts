import { Entity, Column, BaseEntity, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn, JoinColumn, ManyToOne, OneToOne } from "typeorm"
import { Brands } from "./Brands"
import { Countries } from "./Countries"
import { Categories } from "./Categories"
import { ProductDiscount } from "./ProductDiscount"
import { ProductFavorite } from "./ProductFavorite"
import { StockBalance } from "./StockBalance"
import { BadgeBuyDisc } from "./BadgeBuyDisc"
import { BadgeBuyFree } from "./BadgeBuyFree"

@Entity("mapping_products")
export class MappingProducts extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number
    @Column()
    group_name_th: string
    @Column()
    group_name_en: string
    @Column()
    pr_code: string
    @Column()
    pr_name_th: string
    @Column()
    pr_name_en: string
    @Column()
    description_th: string
    @Column()
    description_en: string
    @Column()
    price_normal: number
    @Column()
    member1_price: number
    @Column()
    member2_price: number
    @Column()
    member3_price: number
    @Column()
    member4_price: number
    @Column()
    member5_price: number
    @Column()
    brand: string
    @Column()
    brand_name_th: string
    @Column()
    brand_name_en: string
    @Column()
    brand_image_th: string
    @Column()
    brand_image_en: string
    @Column()
    is_popular: boolean
    @Column()
    country_code: string
    @Column()
    country_name_th: string
    @Column()
    country_name_en: string
    @Column()
    country_image_th: string
    @Column()
    country_image_en: string
    @Column()
    country_image_icon: string
    @Column()
    cat_code: string
    @Column()
    category: string
    @Column()
    sub_category: string
    @Column()
    sub_category_th: string
    @Column()
    sub_category_en: string
    @Column()
    thumbnail: string
    @Column()
    upselling_badge_th: string
    @Column()
    upselling_badge_en: string
    @Column()
    products_badge_th: string
    @Column()
    products_badge_en: string
    @Column()
    promotion_badge_th: string
    @Column()
    promotion_badge_en: string
    @Column()
    service_badge: string
    @Column()
    stock_on_hand: number
    @Column()
    stock_online: number
    @Column()
    product_image_list: string
    @Column()
    product_status: string
    @Column()
    product_type: string
    @Column()
    is_active: boolean
    @Column()
    consumer_unit_th: string
    @Column()
    consumer_unit_en: string
    @Column()
    exclusive_finds_flag: boolean
    @Column()
    best_seller_flag: boolean
    @Column()
    this_week_promotion_flag: boolean
    @Column()
    recommended_flag: boolean
    @Column()
    created_at: Date
    @Column()
    created_by: string
    @Column()
    updated_at: Date
    @Column()
    updated_by: string
    @Column()
    promotion_discount: string
    @Column()
    product_rating: number
    @Column()
    product_created_date: Date
    @Column()
    short_description_th: string
    @Column()
    short_description_en: string
    @Column()
    long_description_th: string
    @Column()
    long_description_en: string
    @Column()
    image: string
    @Column()
    min_stock: number
    @Column()
    safety_stock: number
    @Column()
    limit_per_order: number
    @Column()
    sku: string

    @ManyToOne((_type) => Categories, (item) => item.products)
    @JoinColumn({ name: 'cat_code', referencedColumnName: 'category_code' })
    categoryProduct: Categories

    @ManyToOne((_type) => Countries, (item) => item.products)
    @JoinColumn({ name: 'country_code', referencedColumnName: 'country_code' })
    countryProduct: Countries

    @ManyToOne((_type) => Brands, (item) => item.products)
    @JoinColumn({ name: 'brand', referencedColumnName: 'brand_code' })
    brandProduct: Brands

    @ManyToOne((_type) => ProductDiscount, (item) => item.pr_code)
    @JoinColumn({ name: 'pr_code', referencedColumnName: 'pr_code' })
    productDiscount: ProductDiscount

    @OneToOne((_type) => ProductFavorite, (item) => item.pr_code)
    @JoinColumn({ name: 'pr_code', referencedColumnName: 'pr_code' })
    productFavorite: ProductFavorite

    @ManyToOne((_type) => StockBalance, (item) => item.products)
    @JoinColumn({ name: 'pr_code', referencedColumnName: 'pr_code' })
    stockBalance: StockBalance

    @OneToOne((_type) => BadgeBuyDisc, (item) => item.pr_code)
    @JoinColumn({ name: 'pr_code', referencedColumnName: 'pr_code' })
    badgeBuyDisc: BadgeBuyDisc

    @OneToOne((_type) => BadgeBuyFree, (item) => item.pr_code)
    @JoinColumn({ name: 'pr_code', referencedColumnName: 'pr_code' })
    badgeBuyFree: BadgeBuyFree
}
