import { Entity, Column, BaseEntity, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn } from "typeorm"

@Entity("member_trial")
export class MemberTrial extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number
    @Column()
    period: number
    @Column()
    member_free: number
    @Column()
    title_th: string
    @Column()
    title_en: string
    @Column()
    description_th: string
    @Column()
    description_en: string
    @Column()
    note: string
    @CreateDateColumn()
    created_at: Date
    @UpdateDateColumn()
    updated_at: Date
    @Column()
    created_by: string
    @Column()
    updated_by: string
}
