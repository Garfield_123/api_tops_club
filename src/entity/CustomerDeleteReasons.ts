import { Entity, Column, BaseEntity, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn } from "typeorm"

@Entity("customer_delete_reasons")
export class CustomerDeleteReasons extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number
    @Column()
    name_en: string
    @Column()
    name_th: string
    @Column()
    is_active: boolean
}
