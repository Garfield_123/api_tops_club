import { Entity, Column, BaseEntity, PrimaryGeneratedColumn } from "typeorm"

@Entity("terms_condition")
export class TermsCondition extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number
    @Column()
    description: string
    @Column()
    terms_condition_en: string
    @Column()
    terms_condition_th: string
}
