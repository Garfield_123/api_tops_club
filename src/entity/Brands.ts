import { Entity, Column, BaseEntity, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn, OneToMany, JoinColumn } from "typeorm"
import { MappingProducts } from "./MappingProducts"

@Entity("brands")
export class Brands extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number
    @Column()
    brand_code: string
    @Column()
    brand_name_th: string
    @Column()
    brand_name_en: string
    @Column()
    brand_image_th: string
    @Column()
    brand_image_en: string
    @Column()
    cms_key: string
    @Column()
    is_active: boolean
    @Column()
    is_popular: boolean
    @Column()
    popular_sequence: number
    @Column()
    sequence: number
    @CreateDateColumn()
    created_at: Date
    @Column()
    created_by: string
    @UpdateDateColumn()
    updated_at: Date
    @Column()
    updated_by: string

    @OneToMany((_type) => MappingProducts, (item) => item.brandProduct)
    @JoinColumn({ name: 'brand_code' })
    products: MappingProducts[]
}
