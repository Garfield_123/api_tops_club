import { Entity, Column, BaseEntity, CreateDateColumn, UpdateDateColumn, PrimaryColumn, OneToMany, JoinColumn } from "typeorm"
import { MappingProducts } from "./MappingProducts"

@Entity("badge_buy_disc")
export class BadgeBuyDisc extends BaseEntity {
    @PrimaryColumn()
    store_code: string
    @PrimaryColumn()
    pr_code: string
    @Column()
    disc_msg: string
    @Column()
    on_web: string
    @Column()
    discount_type: string
    @PrimaryColumn()
    ctlid: number
    @Column()
    protype: string
    @Column()
    disc_value: number
    @UpdateDateColumn()
    update_at: Date
    @Column()
    update_by: string
}
