import { Entity, Column, BaseEntity, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn } from "typeorm"

@Entity("customer_segment")
export class CustomerSegment extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number
    @Column()
    segment_udcode: string
    @Column()
    segment_name: string
    @Column()
    discount: number
    @Column()
    segment_level: number
    @Column()
    customer_badge_en: string
    @Column()
    customer_badge_th: string
    @CreateDateColumn()
    created_at: Date
    @Column()
    created_by: string
    @UpdateDateColumn()
    updated_at: Date
    @Column()
    updated_by: string
}
