import { Entity, Column, BaseEntity, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn, JoinColumn, OneToMany } from "typeorm"
import { MappingProducts } from "./MappingProducts"

@Entity("countries")
export class Countries extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number
    @Column()
    badge_id: number
    @Column()
    country_code: string
    @Column()
    country_short: string
    @Column()
    country_name_th: string
    @Column()
    country_name_en: string
    @Column()
    country_image_th: string
    @Column()
    country_image_en: string
    @Column()
    country_banner_th: string
    @Column()
    country_banner_en: string
    @Column()
    cms_key: string
    @Column()
    is_active: boolean
    @Column()
    sequence: number
    @CreateDateColumn()
    created_at: Date
    @Column()
    created_by: string
    @UpdateDateColumn()
    updated_at: Date
    @Column()
    updated_by: string
    @Column()
    country_logo: string

    @OneToMany((_type) => MappingProducts, (item) => item.countryProduct)
    @JoinColumn({ name: 'country_code' })
    products: MappingProducts[]
}
