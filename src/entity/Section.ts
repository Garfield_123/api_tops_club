import { Entity, Column, BaseEntity, UpdateDateColumn, PrimaryColumn, OneToOne, JoinColumn, ManyToOne, CreateDateColumn } from "typeorm"

@Entity("section")
export class Section extends BaseEntity {
    @PrimaryColumn()
    id: number
    @Column()
    screen_id: number
    @Column()
    sequence: number
    @Column()
    title_th: string
    @Column()
    title_en: string
    @Column()
    subtitle_th: string
    @Column()
    subtitle_en: string
    @Column()
    bg_color: string
    @Column()
    type: string
    @Column()
    cms_key: string
    @Column()
    is_active: boolean
    @Column()
    is_manual: boolean
    @CreateDateColumn()
    created_at: Date
    @Column()
    created_by: string
    @UpdateDateColumn()
    updated_at: Date
    @Column()
    updated_by: string
}
