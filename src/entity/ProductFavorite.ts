import { Entity, Column, BaseEntity, UpdateDateColumn, PrimaryColumn, OneToOne, JoinColumn, ManyToOne } from "typeorm"
import { Customer } from "./Customer"

@Entity("product_favorite")
export class ProductFavorite extends BaseEntity {
    @PrimaryColumn()
    pr_code: string
    @PrimaryColumn()
    customer_id: number
    @Column()
    is_favorite: boolean
    @UpdateDateColumn()
    last_updated_at: Date
    @Column()
    qty: number

    @ManyToOne((_type) => Customer, (item) => item.productFavorites)
    @JoinColumn({ name: 'customer_id', referencedColumnName: 'customer_id' })
    customer: Customer
}
