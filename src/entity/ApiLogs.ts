import { Entity, Column, BaseEntity, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn } from "typeorm"

@Entity("api_logs")
export class ApiLogs extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number
    @Column()
    api_route: string
    @Column()
    request: string
    @Column()
    response: string
    @Column()
    system: string
    @Column()
    remark: string
    @Column()
    type: string
    @Column()
    ref: string
    @CreateDateColumn()
    created_at: Date
}
