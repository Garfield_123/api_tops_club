import { Entity, Column, BaseEntity, CreateDateColumn, UpdateDateColumn, PrimaryColumn, OneToMany, JoinColumn } from "typeorm"
import { MappingProducts } from "./MappingProducts"

@Entity("badge_buy_free")
export class BadgeBuyFree extends BaseEntity {
    @PrimaryColumn()
    store_code: string
    @PrimaryColumn()
    pr_code: string
    @Column()
    on_web: string
    @PrimaryColumn()
    ctlid: number
    @Column()
    protype: string
    @Column()
    freegift_msg: string
    @Column()
    freegift_msg_th: string
    @UpdateDateColumn()
    update_at: Date
    @Column()
    update_by: string
}
