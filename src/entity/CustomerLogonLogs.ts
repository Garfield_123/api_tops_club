import { Entity, Column, BaseEntity, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn } from "typeorm"

@Entity("customer_logon_logs")
export class CustomerLogonLogs extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number
    @Column()
    customer_id: number
    @Column()
    logon_at: Date
    @Column()
    logout_at: Date
    @CreateDateColumn()
    created_at: Date
    @UpdateDateColumn()
    updated_at: Date
}
