import { Entity, Column, BaseEntity, UpdateDateColumn, PrimaryColumn, OneToOne, JoinColumn, ManyToOne, CreateDateColumn, ManyToMany } from "typeorm"
import { Customer } from "./Customer"

@Entity("s_orders")
export class Order extends BaseEntity {
    @PrimaryColumn()
    ref: string
    @Column()
    st_code: string
    @Column()
    cus_id: string
    @Column()
    order_date: Date
    @Column()
    nitem: number
    @Column({ type: 'float8' })
    amount: number
    @Column({ type: 'float8' })
    discount: number
    @Column()
    status: string
    @Column()
    dlvdate: Date
    @Column({ type: 'json' })
    jsn_payment: any
    @Column({ type: 'json' })
    jsn_items: any
    @Column()
    confirmpaid: Date
    @Column()
    t1c: string
    @Column()
    jsntr_charge: string
    @Column()
    sale_source: string
    @Column({ type: 'json' })
    jsn_posref: any
    @Column({ type: 'json' })
    jsn_card: any
    @Column()
    req_tax: number
    @Column()
    send_etax: string
    @Column()
    lupd: Date
    @Column({ type: 'json' })
    result: any
    @Column()
    recid: string
    @Column()
    ondate: Date
    @Column({ type: 'json' })
    sum_set: any
    @Column({ type: 'json' })
    pm_result: any
    @Column({ type: 'json' })
    pro_info: any
    @Column()
    wotdisc: string
    @Column()
    wotdiscdet: string
    @Column()
    updated_at: Date
    @Column()
    updated_by: string
    @Column({ type: 'json' })
    json_bill: any
    @Column()
    call_customer_oos: string
    @Column({ type: 'json' })
    jsn_transport: any
    @Column()
    cus_segment: number
    @Column({ type: 'json' })
    jsn_delivery: any
    @Column({ type: 'json' })
    jsn_sts_time_slot: any
    @Column({ type: 'json' })
    jsn_shipping_address: any
    @Column({ type: 'json' })
    coveragearea: any
    @Column()
    order_remark: string
    @Column({ type: 'json' })
    jsn_coupon: any
    @Column({ type: 'json' })
    jsncoupon_result: any
    @Column({ type: 'json' })
    jsncoupon_det_result: any
    @Column({ type: 'json' })
    jsn_plpp_result: any
    @Column()
    t1c_segment_id: string
    @Column()
    t1c_segment_level: string
    @Column({ type: 'numeric' })
    member_saved: number
    @Column()
    trial_registered: boolean

    @ManyToOne((_type) => Customer, (item) => item.customer_id)
    @JoinColumn({ name: 'cus_id', referencedColumnName: 'customer_id' })
    customer: Customer
}
