import { Entity, Column, BaseEntity, UpdateDateColumn, PrimaryColumn, OneToOne, JoinColumn, ManyToOne, CreateDateColumn } from "typeorm"
import { Customer } from "./Customer"

@Entity("send_mails")
export class SendMail extends BaseEntity {
    @PrimaryColumn()
    id: number
    @Column()
    mail_type: string
    @Column()
    ref: string
    @Column()
    customer_id: number
    @Column()
    is_sent: boolean
    @Column()
    send_count: number
    @Column()
    send_at: Date
    @CreateDateColumn()
    created_at: Date
    @UpdateDateColumn()
    updated_at: Date

    @OneToOne((_type) => Customer, (item) => item.customer_id)
    @JoinColumn({ name: 'customer_id', referencedColumnName: 'customer_id' })
    customer: Customer
}
