import { Entity, Column, BaseEntity, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn } from "typeorm"

@Entity("member_benefit")
export class MemberBenefit extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number
    @Column()
    member_trial_id: number
    @Column()
    image_icon: string
    @Column()
    benefit_en: string
    @Column()
    benefit_th: string
    @Column()
    description_th: string
    @Column()
    description_en: string
    @Column()
    type: string
    @CreateDateColumn()
    created_at: Date
    @UpdateDateColumn()
    updated_at: Date
    @Column()
    created_by: string
    @Column()
    updated_by: string
}
