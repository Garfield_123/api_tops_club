import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, CreateDateColumn, UpdateDateColumn } from "typeorm"

@Entity("theone")
export class TheOne extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number
    @Column()
    t1_card_no: string
    @Column()
    t1_points_balance: number
    @Column()
    t1_points_expiry_this_year: number
    @Column()
    t1_date_of_birth: string
    @Column()
    t1_employee_bu_short_code: string
    @Column()
    t1_employee_id: string
    @Column()
    t1_gender: string
    @Column()
    t1_is_staff: string
    @Column()
    t1_member_language_pref: string
    @Column()
    t1_style: string
    @Column()
    t1_title_en: string
    @Column()
    t1_title_th: string
    @Column()
    t1_segments: string
    @Column()
    t1_image_profile: string
    @Column()
    t1_first_name_th: string
    @Column()
    t1_first_name_en: string
    @Column()
    t1_last_name_th: string
    @Column()
    t1_last_name_en: string
    @Column()
    t1_status: string
    @Column()
    t1_card: string
    @Column()
    t1_consent_date: string
    @Column()
    t1_user_account_id: string
    @Column()
    t1_email: string
    @Column()
    t1_email_verified: string
    @Column()
    t1_mobile_country: string
    @Column()
    t1_mobile_value: string
    @Column()
    t1_mobile_verified: string
    @Column()
    t1_consent_flag: string
    @Column()
    t1_consent_version: string
    @Column()
    t1_dcs_consent_version: string
    @Column()
    t1_consent_challenge: string
    @Column()
    t1_refresh_token: string
    @Column()
    is_trial: boolean
    @Column()
    trial_start: Date
    @Column()
    trial_end: Date
    @Column()
    is_member: boolean
    @Column()
    member_start: Date
    @Column()
    member_end: Date
    @Column()
    have_family: boolean
    @Column()
    family_id: number
    @Column()
    have_parent: boolean
    @Column()
    parent_id: number
    @CreateDateColumn()
    created_at: Date
    @Column()
    created_by: string
    @UpdateDateColumn()
    updated_at: Date
    @Column()
    updated_by: string
}
