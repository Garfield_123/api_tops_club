import * as express from "express";
import * as bodyParser from "body-parser";
import { AppDataSource } from "./data-source";
import { DataSource } from "typeorm";
import { setupRepositories } from "./entity";
import * as logger from "morgan";
import * as cors from "cors";
import * as cookieParser from "cookie-parser";
import * as fileUpload from "express-fileupload";
import * as http from 'http'
import { config } from "./config";
import {
  authMiddleware,
  notFoundHandler,
  errorHandler,
  validateFavicon,
} from "./middleware";

import {
  the1Router,
  shopByRouter,
  customerRouter,
  productRouter,
  backendRouter,
  homeRouter,
  // orderRouter,
} from "./route";

AppDataSource.initialize()
  .then(async (orm: DataSource) => {
    const repos = setupRepositories(orm);
    global.repos = repos;
    global.rawSql = orm

    const app = express();
    const corsOptions = {
      methods: ["GET", "PUT", "POST", "DELETE"],
      origin: "*",
      optionsSuccessStatus: 204,
      maxAge: 3600,
    };
    const urlencoded = bodyParser.urlencoded({ extended: true });
    const jsonParser = bodyParser.json({ limit: "50mb" });

    app.use(cors(corsOptions));
    app.use(logger("dev"));
    app.use(cookieParser());
    app.use(jsonParser);
    app.use(urlencoded);
    app.use(fileUpload());
    app.use(validateFavicon);
    app.use(
      authMiddleware().unless({
        path: [
          `/`,
          '/the1/auth',
          '/the1/request-otp',
          '/the1/login-otp',
          '/the1/refresh',
          '/customer/check-member',
          '/customer/notification',
          '/customer/benefit',
          '/customer/terms-and-conditions',
          '/backend/coupon',
          '/shopby/plp',
          '/shopby/filter',
          '/shopby',
          '/product/agoliasearch',
          '/home',
          new RegExp('/product/detail/'),
        ],
      })
    );

    app.get("/", async (req, res) => {
      res.status(200).json({
        data: "Ok!!!",
      });
    });

    app.use("/the1", the1Router);
    app.use("/shopby", shopByRouter);
    app.use("/customer", customerRouter);
    app.use("/product", productRouter);
    app.use("/backend", backendRouter);
    app.use("/home", homeRouter);
    app.route("/*").all(notFoundHandler);
    app.use(errorHandler);

    const server = http.createServer(app);
    const io = require('socket.io')(server, {
        cors: {
            origin: "*",
            methods: ["GET", "POST"]
        }
    });
    global.io = io;

    io.on('connection', (socket) => { 
        socket.on('data', (data) => {
            console.log(`received data: ${data}`);
        });

        socket.on('disconnect', () => {
            console.log('user disconnected');
        });
    });

    server.listen(config.port, async () => {
      console.log(`Server listening on port ${config.port}`);
    });
  })
  .catch((error) => console.log(error));
