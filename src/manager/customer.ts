import { ProductFavorite } from "../entity/ProductFavorite";
import { Customer } from "../entity/Customer";
import { manager } from ".";
import { CustomerSegment } from "../entity/CustomerSegment";
import {
  addYears,
  addDays,
  differenceInCalendarDays,
  addMonths,
  format,
  addHours,
} from "date-fns";
import { ENotiType, ESegment } from "../types/customer";
import { EMailType } from "./sendMail";
import { CustomerLogonLogs } from "../entity/CustomerLogonLogs";
import { IsNull } from "typeorm";
import { DUMMY_CUSTOMER_ID } from "../utils/constants";
import { obsClient } from "../utils/obs";
import { CustomerDeleteAccountLogs } from "../entity/CustomerDeleteAccountLogs";

export class CustomerManager {
  async changeCustomerEmail(customerId: number, newEmail: string) {
    try {
      const repos = global.repos;
      let data = await repos.customer.findOne({
        where: {
          customer_id: customerId,
        },
      });
      data.email = newEmail;
      await data.save();
      return data;
    } catch (error) {
      throw error;
    }
  }
  async checkMember(mobile: string, segment?: string) {
    try {
      const repos = global.repos;
      const condition: {
        mobile: string;
        segment_code?: string;
      } = {
        mobile: mobile
      }

      if (segment) condition.segment_code = segment;
      const data = await repos.customer.findOne({
        where: condition
      });
      return data;
    } catch (e) {
      throw e;
    }
  }

  async register(customerId: number, isTrial: boolean): Promise<any> {
    try {
      const repos = global.repos;
      const segmentCode = isTrial ? ESegment.TRIAL : ESegment.MEMBER;
      const customerInfo = await manager.customer.findByCustomerId(customerId);
      let updateOption: any = {
        is_trial: isTrial,
        segment_code: segmentCode,
        updated_at: new Date(),
      };

      let memberExpired = isTrial
        ? addDays(addMonths(new Date(), 1), -1)
        : addDays(addYears(new Date(), 1), -1);
      updateOption.membership_expired_date = memberExpired;
      updateOption.membership_enrolled_date = new Date();
      updateOption.is_membership_cancelled = false;
      updateOption.membership_status = true;
      updateOption.is_active = true;

      if (!customerInfo?.membership_register_date) {
        updateOption.membership_register_date = new Date();
      }

      await repos.customer
        .createQueryBuilder()
        .update()
        .set(updateOption)
        .where("customer_id = :customerId", {
          customerId,
        })
        .execute();

      await manager.sendMail.save(EMailType.NEW_MEMBER, null, customerId);

      if (customerInfo?.t1_card_no) {
        if (customerInfo?.segment_code) {
          await manager.the1.deleteCentralThe1Segment(
            customerInfo.t1_card_no,
            customerInfo.segment_code
          );
        }
        await manager.the1.updateCentralThe1Segment(
          customerInfo.t1_card_no,
          segmentCode
        );
      }

      const segment = await manager.customer.findSegment(segmentCode);
      return {
        is_trial: isTrial,
        is_member_expired: false,
        member_expired_date: memberExpired,
        member_remainning_days:
          differenceInCalendarDays(memberExpired, addHours(new Date(), 7)) + 1,
        renew_member_date: addDays(memberExpired, 1),
        segment: segmentCode,
        segment_name: segment?.segment_name,
      };
    } catch (e) {
      throw e;
    }
  }

  async findSegment(segment?: string): Promise<any> {
    try {
      const repos = global.repos;
      const whereCause: any = {};
      if (segment) whereCause.segment_udcode = segment;
      else whereCause.segment_level = 0;

      const data = await repos.customerSegment.findOne({
        where: whereCause,
      });
      return data;
    } catch (e) {
      throw e;
    }
  }

  async findByCustomerId(customerId: number): Promise<Customer> {
    try {
      const repos = global.repos;
      if (!customerId) return null;
      const data = await repos.customer.findOne({
        where: {
          customer_id: customerId,
        },
      });

      return data;
    } catch (e) {
      throw e;
    }
  }

  async saveProductFavorite(
    customer: Customer,
    prCode: string,
    isFavorite: boolean,
    qty: number
  ): Promise<void> {
    try {
      const repos = global.repos;
      const productFavorite = new ProductFavorite();
      productFavorite.customer_id = customer.customer_id;
      productFavorite.pr_code = prCode;
      productFavorite.is_favorite = isFavorite;
      productFavorite.last_updated_at = new Date();
      productFavorite.qty = qty || 1;
      await repos.productFavorite.save(productFavorite);
    } catch (e) {
      throw e;
    }
  }

  async getProductFavorite(
    customer: Customer,
    customerSegment: CustomerSegment
  ): Promise<any> {
    try {
      const repos = global.repos;
      let data = await repos.mappingProducts
        .createQueryBuilder("mp")
        .distinct(true)
        .select([
          "mp.id as id",
          "mp.pr_code as pr_code",
          "mp.pr_name_th as pr_name_th",
          "mp.pr_name_en as pr_name_en",
          "mp.description_th as description_th",
          "mp.description_en as description_en",
          "mp.price_normal as price_normal",
          "mp.brand as brand",
          "mp.brand_name_th as brand_name_th",
          "mp.brand_name_en as brand_name_en",
          "mp.brand_image_th as brand_image_th",
          "mp.brand_image_en as brand_image_en",
          "mp.is_popular as is_popular",
          "mp.country_code as country_code",
          "mp.country_name_th as country_name_th",
          "mp.country_name_en as country_name_en",
          "mp.country_image_th as country_image_th",
          "mp.country_image_en as country_image_en",
          "mp.country_image_icon as country_image_icon",
          "mp.thumbnail as thumbnail",
          "mp.upselling_badge_th as upselling_badge_th",
          "mp.upselling_badge_en as upselling_badge_en",
          "mp.products_badge_th as products_badge_th",
          "mp.products_badge_en as products_badge_en",
          "mp.promotion_badge_th as promotion_badge_th",
          "mp.promotion_badge_en as promotion_badge_en",
          "mp.service_badge as service_badge",
          "mp.stock_on_hand as stock_on_hand",
          "mp.stock_online as stock_online",
          "mp.product_status as product_status",
          "mp.product_type as product_type",
          "mp.is_active as is_active",
          "mp.consumer_unit_th as consumer_unit_th",
          "mp.consumer_unit_en as consumer_unit_en",
          "mp.exclusive_finds_flag as exclusive_finds_flag",
          "mp.best_seller_flag as best_seller_flag",
          "mp.this_week_promotion_flag as this_week_promotion_flag",
          "mp.recommended_flag as recommended_flag",
          "mp.promotion_discount as promotion_discount",
          "mp.product_rating as product_rating",
          "mp.product_created_date as product_created_date",
          "mp.short_description_th as short_description_th",
          "mp.short_description_en as short_description_en",
          "mp.long_description_th as long_description_th",
          "mp.long_description_en as long_description_en",
          "mp.image as image",
          "mp.min_stock as min_stock",
          "mp.safety_stock as safety_stock",
          "pf.qty as productFavoriteQty",
          "pf.is_favorite as is_favorite",
          "cp.percent_discount as category_percent_discount",
          "pd.percent_discount as product_percent_discount",
          "bbd.disc_value as bbd_disc_value",
          "bbd.discount_type as bbd_discount_type",
          "bbf.freegift_msg as bbf_freegift_msg",
          "bbf.freegift_msg_th as bbf_freegift_msg_th"
        ])
        .innerJoin("mp.categoryProduct", "cp")
        .leftJoin("mp.productDiscount", "pd")
        .innerJoin(
          "mp.productFavorite",
          "pf",
          "pf.customer_id = :customerId and pf.is_favorite = true",
          {
            customerId: customer?.customer_id || 0,
          }
        )
        .leftJoin("mp.badgeBuyDisc", "bbd")
        .leftJoin("mp.badgeBuyFree", "bbf")
        .getRawMany();

      data = await manager.product.setDetail(data, customerSegment);
      return data || [];
    } catch (e) {
      throw e;
    }
  }

  async saveCustomerDetail(
    data: any,
    access: string,
    refresh: string,
  ): Promise<Customer> {
    try {
      const repos = global.repos;
      const {
        onlineMobile,
        cards,
        firstName,
        lastName,
        onlineEmail,
        employeeID,
        isStaff,
        userAccountID,
      } = data;

      if (!onlineMobile) return;

      const { country, value } = onlineMobile;
      const mobile = `${country}${value}`;
      let customerExists = await this.checkMember(mobile);
      const hasCardNo = cards && cards.length > 0;

      const customerData = new Customer();
      if (customerExists) {
        customerData.customer_id = customerExists.customer_id;
        customerData.updated_by = "The1";
        customerData.updated_at = new Date();
      } else {
        customerData.email = onlineEmail ? onlineEmail.value : null;
        customerData.membership_status = true;
        customerData.membership_expired_date = null;
        customerData.billing_address = "{}";
        customerData.shipping_address = "{}";
        customerData.mobile = mobile;
        customerData.created_by = "The1";
        customerData.segment_code = ESegment.GUESS;
        customerData.first_name = firstName
          ? firstName.th
          : customerExists.first_name;
        customerData.first_name_en = firstName
          ? firstName.en
          : customerExists.first_name_en;
        customerData.last_name = lastName
          ? lastName.th
          : customerExists.last_name;
        customerData.last_name_en = lastName
          ? lastName.en
          : customerExists.last_name_en;
      }
      
      customerData.t1_card_no = hasCardNo ? cards[0].cardNo : null;
      customerData.t1_points_balance = hasCardNo ? cards[0].pointsBalance : 0;
      customerData.is_staff = isStaff || "N";
      customerData.employee_id = employeeID || null;
      customerData.t1_access_token = access;
      customerData.t1_refresh_token = refresh;
      customerData.t1_user_account_id = userAccountID;
      await repos.customer.save(customerData);

      const customer = await this.checkMember(mobile);
      return customer;
    } catch (e) {
      throw e;
    }
  }

  async clearTokenThe1(customerId: number): Promise<any> {
    try {
      const repos = global.repos;
      await repos.customer.update(
        {
          customer_id: customerId,
        },
        {
          t1_access_token: null,
          t1_refresh_token: null,
        }
      );
    } catch (e) {
      throw e;
    }
  }

  async saveNotification(
    customerId: number,
    notiType: ENotiType,
    isNoti: boolean
  ) {
    try {
      const repos = global.repos;
      let colUpdate = "is_notification";
      switch (notiType) {
        case ENotiType.EMAIL:
          colUpdate = "is_email";
          break;
        case ENotiType.SMS:
          colUpdate = "is_sms";
          break;
        default:
      }
      await repos.customer.update(
        {
          customer_id: customerId,
        },
        {
          [colUpdate]: isNoti,
        }
      );
    } catch (e) {
      throw e;
    }
  }

  async updateLogonLog(customerId: number, isLogin: boolean) {
    try {
      const dt = new Date();
      const repos = global.repos;
      if (!isLogin) {
        await repos.customerLogonLog.update(
          {
            customer_id: customerId,
            logout_at: IsNull(),
          },
          {
            logout_at: dt,
            updated_at: dt,
          }
        );
      } else {
        const newLogon = new CustomerLogonLogs();
        newLogon.customer_id = customerId;
        newLogon.logon_at = dt;
        newLogon.created_at = dt;
        newLogon.updated_at = dt;
        await repos.customerLogonLog.save(newLogon);
      }
    } catch (e) {
      throw e;
    }
  }

  async getMembershipInfo(customer: Customer) {
    try {
      const repos = global.repos;
      let result: {
        isShowRenewalNotice?: boolean;
        is_trial?: boolean;
        renew_member_date?: Date;
        segment?: string;
        segment_name?: string;
        is_cancelled?: boolean;
        is_member_expired?: boolean;
        member_expired_date?: Date;
        member_remainning_days?: number;
        total_member_saved?: number;
        member_lifetime_start?: string;
        member_lifetime_end?: string;
        customer?: Customer
      } = {
        isShowRenewalNotice: false,
      };

      const totalMemberSaved = await repos.order
        .createQueryBuilder("order")
        .select("SUM(order.member_saved)", "total")
        .where("order.cus_id = :customerId", {
          customerId: customer.customer_id,
        })
        .groupBy("order.cus_id")
        .getRawOne();

      if (customer) {
        let segmentCode = customer.segment_code;
        let memberExpiredDate = customer.membership_expired_date;
        let isTrial = customer.is_trial;
        let isCancelled = customer.is_membership_cancelled;
        if (customer.is_member_renewing) {
          memberExpiredDate = addYears(new Date(memberExpiredDate), 1);
          isTrial = false;
          segmentCode = ESegment.MEMBER;
          isCancelled = false;
        }

        const segment = await manager.customer.findSegment(segmentCode);
        result = {
          is_trial: isTrial,
          renew_member_date: addDays(memberExpiredDate, 1),
          segment: segmentCode,
          segment_name: segment?.segment_name,
          is_cancelled: isCancelled,
          is_member_expired:
            differenceInCalendarDays(
              memberExpiredDate,
              addHours(new Date(), 7)
            ) +
              1 <=
            0,
          member_expired_date: memberExpiredDate,
          member_remainning_days:
            differenceInCalendarDays(
              memberExpiredDate,
              addHours(new Date(), 7)
            ) + 1,
          total_member_saved: Number(totalMemberSaved?.total || 0),
          member_lifetime_start: format(new Date(customer?.membership_register_date || null), "yyyy-MM-dd"),
          member_lifetime_end: format(new Date(addHours(new Date(), 7) || null), "yyyy-MM-dd"),
          customer,
        };
      }

      return result;
    } catch (e) {
      throw e;
    }
  }

  async removeCustomer(
    customerId: number, 
    isCancel: boolean,
    fromRegister: boolean, 
    isForceDelete: boolean = false, 
    reasons: {
      id: number;
      remark?: string;
    }[] = []
  ) {
    try {
      let customerInfo: Customer = await this.findByCustomerId(customerId);

      const repos = global.repos;
      if (!customerInfo) return

      if (isCancel) {
        await repos.customer.update({
          customer_id: customerInfo.customer_id,
        }, {
          is_delete_account: false,
          request_delete_at: null,
        });

        return
      }

      if (fromRegister) {
        await repos.customer.delete({
          customer_id: customerId,
        });
      } else {
        await repos.customer.update({
          customer_id: customerInfo.customer_id,
        }, {
          is_delete_account: true,
          request_delete_at: new Date(),
        });

        for (const reason of reasons) {
          if (!reason?.id) continue;
  
          const newLogs = new CustomerDeleteAccountLogs();
          newLogs.customer_id = customerId;
          newLogs.reason_id = reason.id;
          newLogs.remark = reason?.remark;
          newLogs.created_at = new Date();
          newLogs.updated_at = new Date();
          await repos.customerDeleteAccountLog.save(newLogs)
        }

        if (isForceDelete) {
          await this.deleteCustomer(customerId);
        } 
      }
    } catch (e) {
      throw e;
    }
  }

  async deleteCustomer (customerId: number) {
    try {
      const repos = global.repos;
      await this.saveCustomerDeleted(customerId);
      await repos.customer.delete({
        customer_id: customerId,
      });
    } catch (e) {
      throw e;
    }
  }

  async saveCustomerDeleted (customerId: number) {
    try {
      let customerInfo: Customer = await this.findByCustomerId(customerId);
      delete customerInfo?.created_by;
      delete customerInfo?.updated_at;
      delete customerInfo?.updated_by;
      
      const repos = global.repos;
      await repos.customerDeleted.insert({
        ...customerInfo
      })
    } catch (e) {
      throw e;
    }
  }

  async getMultiAddress(customerId: number) {
    try {
      let customerInfo: Customer = await this.findByCustomerId(customerId);

      if (!customerInfo) {
        return [];
      }
      return customerInfo?.multiaddress ? JSON.parse(customerInfo.multiaddress) : null;
    } catch (e) {
      throw e;
    }
  }

  async addMultiAddress(customerId: number, body: {
    ADDR: any;
  }): Promise<void> {
    try {
      const { ADDR } = body;
      let now = new Date();
      const repos = global.repos;
      await repos.customer.update({
        customer_id: customerId
      }, {
        multiaddress: JSON.stringify(ADDR),
        updated_at: now,
        updated_by: "App",
      });

      for (const item of ADDR) {
        if (item.adrstt == "ship") {
          await repos.customer.update({
            customer_id: customerId
          }, {
            shipping_address: item,
            updated_at: now,
            updated_by: "App",
          });
        }
        if (item.adrstt == "bill") {
          await repos.customer.update({
            customer_id: customerId
          }, {
            billing_address: item,
            updated_at: now,
            updated_by: "App",
          });
        }
        if (item.adrstt == "all") {
          await repos.customer.update({
            customer_id: customerId
          }, {
            shipping_address: item,
            billing_address: item,
            updated_at: now,
            updated_by: "App",
          });
        }
      }
    } catch (e) {
      throw e
    }
  }

  async deleteMultiAdress(customerId: number, body: {
    id: any;
  }): Promise<void> {
    try {
      const { id: addressId } = body
      const repos = global.repos;
      let customerInfo: Customer = await this.findByCustomerId(customerId);
      const multiAddresses = [];
      let removeAddressType = null;
      for (const item of JSON.parse(customerInfo.multiaddress)) {
        if (item.id != addressId) {
          multiAddresses.push(item);
        } else {
          removeAddressType = item.adrstt;
        }
      }

      await repos.customer.update({
        customer_id: customerId
      }, {
        multiaddress: JSON.stringify(multiAddresses),
        updated_at: new Date(),
        updated_by: "App",
      });

      if (!removeAddressType) {
        const removeColumn = removeAddressType === 'bill' ? 'billing_address' : 'shipping_address';
        await repos.customer.update({
          customer_id: customerId
        }, {
          [removeColumn]: null,
          updated_at: new Date(),
          updated_by: "App",
        });
      }
    } catch (e) {
      throw e
    }
  }

  async getBenefit(type: string) {
    try {
      const repos = global.repos;
      let memberTrials: any = await repos.memberTrial.find({
        where: {
          id: ['trial'].includes(type) ? 2 : 1
        }
      })

      await Promise.all(
        memberTrials.map(async (memberTrial) => {
          memberTrial.sub = [];

          const memberBenefits: any = await repos.memberBenefit.find({
            where: {
              member_trial_id: 1
            }, 
            order: {
              id: 'ASC'
            }
          });
  
          await Promise.all(
            memberBenefits.map(async (benefit) => {
              memberTrial.sub.push({
                id: benefit.id,
                member_trial_id: benefit.member_trial_id,
                image_icon: benefit.image_icon,
                benefit_en: benefit.benefit_en,
                benefit_th: benefit.benefit_th,
                description_en: benefit.description_en,
                description_th: benefit.description_th,
              });
            })
          );
        })
      );
      return memberTrials;
    } catch (e) {
      throw e;
    }
  }

  async saveDeviceId (customerId: number, deviceId?: string): Promise<void> {
    try {
      const repos = global.repos;
      await repos.customer.update({
        customer_id: customerId
      }, {
        last_device_id: deviceId || null,
        last_device_logon_date: new Date(),
        updated_at: new Date(),
        updated_by: "App",
      });
    } catch (e) {
      throw e;
    }
  }

  async getTermsAndConditions(type: string) {
    try {
      const repos = global.repos;
      const result = await repos.termsCondition.findOne({
        where: {
          id: type === 'member' ? 1 : 2
        }
      })
      return result;
    } catch (e) {
      throw e;
    }
  }

  async updateProfileImage(customerId: number, files: any): Promise<void> {
    try {
      const repos = global.repos;
      let image = null;
      if (files) {
        image = await obsClient.putObject(
          "customer_profile",
          files?.customerImage
        );
      }

      await repos.customer.update({
        customer_id: customerId
      }, {
        customer_image: image
      });
    } catch (e) {
      throw e;
    }
  }
}
