import { The1Manager } from "./the1";
import { ShopByManager } from "./shopBy";
import { CustomerManager } from "./customer";
import { ProductManager } from "./product";
import { SendMailManager } from "./sendMail";
import { ApiLogManager } from "./apiLog";
import { BackendManager } from "./backend";
import { HomeManager } from "./home";
import { SectionManager } from "./section";

export const manager = {
    the1: new The1Manager(),
    shopBy: new ShopByManager(),
    customer: new CustomerManager(),
    product: new ProductManager(),
    sendMail: new SendMailManager(),
    apiLog: new ApiLogManager(),
    backend: new BackendManager(),
    home: new HomeManager(),
    section: new SectionManager(),
}
