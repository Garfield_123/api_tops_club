import { manager } from "./";
import { CustomerSegment } from "../entity/CustomerSegment";
import algoliasearch from "algoliasearch";
import { config } from "../config/index";
import { ESegment } from "../types/customer";
import { BadgeBuyDiscType } from "../types/shopBy";

export class ProductManager {
  async calculatePriceDiscount(
    customerSegment: CustomerSegment,
    prCode: string,
    priceNormal: number
  ) {
    try {
      let priceDiscount = 0.0;
      const repos = global.repos;
      const badgeBuyDisc: any = await repos.badgeBuyDisc.findOne({
        where: {
          pr_code: prCode,
        },
      });

      const badgeBuyFree: any = await repos.badgeBuyFree.findOne({
        where: {
          pr_code: prCode,
        },
      });

      if (badgeBuyDisc) {
        const discount = Number(badgeBuyDisc.disc_value.replace(/\$/, ""));
        if (badgeBuyDisc.discount_type === BadgeBuyDiscType.PERCENT) {
          priceDiscount = Number(((discount * priceNormal) / 100).toFixed(2));
        } else if (badgeBuyDisc.discount_type === BadgeBuyDiscType.BATH) {
          priceDiscount = discount;
        }
      } else if (badgeBuyFree) {
        priceDiscount = 0;
      } else {
        priceDiscount = customerSegment
          ? Number(((customerSegment.discount * priceNormal) / 100).toFixed(2))
          : priceDiscount;
      }
      return priceDiscount;
    } catch (e) {
      throw e;
    }
  }

  async setDetail(data: any, customerSegment: CustomerSegment) {
    try {
      const result = await Promise.all(
        data.map(async (product) => {
          await Promise.all(
            Object.keys(product).map((key) => {
              if (
                [
                  "price_normal",
                  "member1_price",
                  "member2_price",
                  "member3_price",
                  "member4_price",
                  "member5_price",
                  "product_rating",
                  "min_stock",
                  "safety_stock",
                  "limit_per_order",
                  "stock_online",
                ].includes(key)
              ) {
                product[key] = isNaN(product[key])
                  ? product[key]
                  : Number(product[key]);
              }
            })
          );
          const priceDiscount = await manager.product.calculatePriceDiscount(
            customerSegment,
            product.pr_code,
            product.price_normal
          );
          product.price_discount = priceDiscount || 0;
          product.price_net = Math.ceil(
            product.price_normal - product.price_discount
          );
          product.customer_badge_en = customerSegment?.customer_badge_en;
          product.customer_badge_th = customerSegment?.customer_badge_th;
          product.is_favorite =
            product?.productFavorite?.is_favorite ||
            product?.is_favorite ||
            false;
          product.the1_point = Math.floor(product.price_net / 50);
          product.is_similar_stock =
            (product.stock_online || 0) >= product.min_stock &&
            (product.stock_online || 0) <= product.safety_stock;
          product.category_percent_discount =
            product?.categoryProduct?.percent_discount ||
            product?.category_percent_discount ||
            0;
          product.product_percent_discount =
            product?.productDiscount?.percent_discount ||
            product?.product_percent_discount ||
            0;

          if (product?.badgeBuyDisc) {
            product.is_redhot = 1;
          } else if (product?.bbd_disc_value) {
            product.is_redhot = 1;
            product.badgeBuyDisc = {
              disc_value: product?.bbd_disc_value,
              discount_type: product?.bbd_discount_type,
            };
          } else {
            product.is_redhot = 0;
            product.badgeBuyDisc = null;
          }

          if (product?.badgeBuyFree) {
            product.is_freegift = 1;
          } else if (product?.bbf_freegift_msg) {
            product.is_freegift = 1;
            product.badgeBuyFree = {
              freegift_msg: product?.bbf_freegift_msg,
              freegift_msg_th: product?.bbf_freegift_msg_th,
            };
          } else {
            product.is_freegift = 0;
            product.badgeBuyFree = null;
          }

          delete product?.category_percent_discount;
          delete product?.product_percent_discount;
          return product;
        })
      );
      return result;
    } catch (e) {
      throw e;
    }
  }
  async algoliaSearch(query, store_id, limit, page) {
    const searchAlgoria = async (
      query: string,
      store_id: string,
      limit: number,
      page: number
    ): Promise<any> => {
      try {
        const client = algoliasearch(
          String(config.algoliaKey.ALGOLIA_CLIENT_ID),
          String(config.algoliaKey.ALGOLIA_CLIENT_SECRET)
        );

        const index = client.initIndex(String(config.algoliaKey.ALGOLIA_INDEX));

        let typos = [];

        let matched_words = [];
        const response = await index.search(query, {
          getRankingInfo: true,
          //   analytics: true,
          //   clickAnalytics: true,
          //   enableABTest: false,
          //   attributesToRetrieve: "*",
          //   attributesToSnippet: "*:20",
          //   snippetEllipsisText: "…",
          //   responseFields: "*",
          // explain: "*",
          //   facets: ["*"],
          filters: `_tags:${store_id}`,
          hitsPerPage: limit,
          page: page - 1,
          typoTolerance: true,
        });

        const hits = response.hits;

        // if (hits.length > 0) {
        //   hits.forEach((element: any) => {
        //     // console.log(element._highlightResult.pr_name_th_wordcut);

        //     // console.log(element._highlightResult.pr_name_en);

        //     const typo_th = element._highlightResult.pr_name_th_wordcut.value;

        //     const typo_en = element._highlightResult.pr_name_en.value;

        //     const matched_words_pr_name_th =
        //       element._highlightResult.pr_name_th.matchedWords;

        //     const matched_words_pr_name_en =
        //       element._highlightResult.pr_name_en.matchedWords;

        //     const matched_words_brand_name_th =
        //       element._highlightResult.brand_name_th.matchedWords;

        //     const matched_words_brand_name_en =
        //       element._highlightResult.brand_name_en.matchedWords;

        //     const matched_words_pr_name_th_wordcut =
        //       element._highlightResult.pr_name_th_wordcut.matchedWords;

        //     const matched_words_cat_name_en =
        //       element._highlightResult.cat_name_en.matchedWords;

        //     const matched_words_cat_name_th_wordcut =
        //       element._highlightResult.cat_name_th_wordcut.matchedWords;

        //     const matched_words_short_description_en =
        //       element._highlightResult.short_description_en.matchedWords;

        //     const matched_words_short_description_th_wordcut =
        //       element._highlightResult.short_description_th_wordcut
        //         .matchedWords;

        //     const matched_words_country_name =
        //       element._highlightResult.country_name.matchedWords;

        //     const input = [
        //       matched_words_pr_name_th,
        //       matched_words_pr_name_en,
        //       matched_words_brand_name_th,
        //       matched_words_brand_name_en,
        //       matched_words_pr_name_th_wordcut,
        //       matched_words_cat_name_en,
        //       matched_words_cat_name_th_wordcut,
        //       matched_words_short_description_en,
        //       matched_words_short_description_th_wordcut,
        //       matched_words_country_name,
        //     ];

        //     input.forEach((arr) => {
        //       matched_words = matched_words.concat(arr);
        //     });

        //     matched_words = [...new Set(matched_words)];

        //     const regex = /<em>(.*?)<\/em>/;

        //     const match_th = typo_th.match(regex);

        //     const match_en = typo_en.match(regex);

        //     if (match_th && match_th.length > 1) {
        //       if (!typos.includes(match_th[1])) {
        //         typos.push(match_th[1]);
        //       }
        //     }

        //     if (match_en && match_en.length > 1) {
        //       if (!typos.includes(match_en[1])) {
        //         typos.push(match_en[1]);
        //       }
        //     }
        //   });
        // }
        let skuArray = [];
        for (let items of hits) {
          skuArray.push(items["sku"]);
        }
        return { skuArray, hits };
      } catch (err) {
        console.error(err);

        return false;
      }
    };
    let agoliaSearchResult = searchAlgoria(query, store_id, limit, page);
    return agoliaSearchResult;
  }

  async getDetail(req: any, res: any) {
    try {
      const { mappingProductId } = req.params;
      const { customerId }: any = req.body;
      const repos = global.repos;

      if (!mappingProductId) {
        return res.status(400).json({
          code: 400,
          message: "Product does not found.",
          data: {},
        });
      }

      const customer = customerId
        ? await manager.customer.findByCustomerId(customerId)
        : null;
      const customerSegment = await manager.customer.findSegment(
        customer?.segment_code || ESegment.GUESS
      );
      let data = await repos.mappingProducts
        .createQueryBuilder("mp")
        .innerJoinAndSelect("mp.categoryProduct", "cp")
        .leftJoinAndSelect("mp.productDiscount", "pd")
        .leftJoinAndSelect(
          "mp.productFavorite",
          "pf",
          "pf.customer_id = :customerId",
          {
            customerId: customer?.customer_id || 0,
          }
        )
        .leftJoinAndSelect("mp.badgeBuyDisc", "bbd")
        .leftJoinAndSelect("mp.badgeBuyFree", "bbf")
        .where("mp.id = :mappingProductId", {
          mappingProductId,
        })
        .getMany();

      data = await this.setDetail(data, customerSegment);
      if (!(data && data.length > 0)) {
        return res.status(400).json({
          code: 400,
          message: "Product does not found.",
          data: {},
        });
      }
      return data[0];
    } catch (e) {
      console.log(e);
      throw e;
    }
  }
}
