import { EApiLogType, IReqSaveLog, IReqSaveLogError, IReqSaveLogRequest, IReqSaveLogResponse } from "../types/apiLog";
import { ApiLogs } from "../entity/ApiLogs";

export class ApiLogManager {
    async saveLog (data?: IReqSaveLog): Promise<any> {
        try {
            const repos = global.repos;
            const newLog = new ApiLogs();
            newLog.api_route = data.apiRoute;
            newLog.ref = data?.ref || null;
            newLog.remark = data.remark;
            newLog.request = data.request;
            newLog.response = data.response;
            newLog.system = data.system;
            newLog.type = data.type || EApiLogType.INFO;
            newLog.created_at = new Date();
            await repos.apiLog.save(newLog);
        } catch (e) {
            throw e;
        }
    }

    async saveLogRequest (data?: IReqSaveLogRequest): Promise<ApiLogs> {
        try {
            const repos = global.repos;
            const newLog = new ApiLogs();
            newLog.ref = data?.ref || null;
            newLog.api_route = data.apiRoute;
            newLog.system = data.system;
            newLog.type = data.type || EApiLogType.INFO;
            newLog.request = data.request;
            newLog.remark   = data.remark;
            newLog.created_at = new Date();
            const result = await repos.apiLog.save(newLog);
            return result;
        } catch (e) {
            throw e;
        }
    }

    async saveLogResponse (data?: IReqSaveLogResponse): Promise<void> {
        try {
            const repos = global.repos;
            await repos.apiLog.update({
                id: data.id,
            }, {
                response: data.response,
            })
        } catch (e) {
            throw e;
        }
    }

    async saveLogError (data?: IReqSaveLogError): Promise<void> {
        try {
            const repos = global.repos;
            await repos.apiLog.insert({
                ref: data?.ref || null,
                api_route: data.apiRoute,
                system: data.system,
                type: EApiLogType.ERROR,
                request: data.request,
                response: data.response,
                remark: data.remark,
                created_at: new Date(),
            })
        } catch (e) {
            throw e;
        }
    }
}
