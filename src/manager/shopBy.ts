import * as _ from "lodash";
import {
  ELanguage,
  EOrderBy,
  EShopByTab,
  ESortByType,
  IConditionShopByPLP,
  CountryFilterList,
  IRequestPLPDetail,
  BrandFilterList,
  BrandInfoFilterList,
  CategoryFilterList,
} from "../types/shopBy";
import { manager } from "./";
import { CustomerSegment } from "../entity/CustomerSegment";
import { Customer } from "../entity/Customer";
import { MappingProducts } from "../entity/MappingProducts";
import { Categories } from "../entity/Categories";
import { ESegment } from "../types/customer";
import { Brackets } from "typeorm";

export class ShopByManager {
  async getPageDetail(lang: string, branchId?: string) {
    try {
      const category = await this.getCategoryDetails(lang, branchId);
      const country = await this.getCountryDetails(lang, branchId);
      const brand = await this.getBrandDetails(lang, branchId);
      return {
        categories: category,
        countries: country,
        brands: brand,
      };
    } catch (e) {
      throw e;
    }
  }

  async getCategoryDetails(lang: string, branchId?: string) {
    try {
      const repos = global.repos;
      let catLv1Sql = repos.categories
        .createQueryBuilder("cat")
        .distinct(true)
        .innerJoin("cat.products", "products")
        .innerJoin(
          Categories,
          "SubCategoryProduct",
          "SubCategoryProduct.category_code = products.category and SubCategoryProduct.is_active = true"
        )
        .where("cat.category_root_id is null and cat.is_active = true")
        .andWhere("COALESCE(products.stock_online, 0) > 0")
        .andWhere("COALESCE(products.stock_online, 0) >= products.min_stock")
        .orderBy("cat.sequence", "ASC");

      const catLv1Lists = await catLv1Sql.getMany();
      return catLv1Lists;
    } catch (e) {
      throw e;
    }
  }

  async getCountryDetails(lang: string, branchId?: string) {
    try {
      const repos = global.repos;
      let sql = repos.countries
        .createQueryBuilder("c")
        .distinct(true)
        .innerJoin("c.products", "products")
        .innerJoin(
          "products.categoryProduct",
          "categoryProduct",
          "categoryProduct.is_active = true"
        )
        .innerJoin(
          Categories,
          "SubCategoryProduct",
          "SubCategoryProduct.category_code = products.category and SubCategoryProduct.is_active = true"
        )
        .leftJoin("products.stockBalance", "stockBalance")
        .where("c.is_active = true")
        .andWhere("COALESCE(products.stock_online, 0) > 0")
        .andWhere("COALESCE(products.stock_online, 0) >= products.min_stock")
        .orderBy("c.sequence", "ASC");

      const countries = await sql.getMany();
      return countries;
    } catch (e) {
      throw e;
    }
  }

  async getBrandDetails(lang: string, branchId?: string) {
    try {
      const repos = global.repos;
      let sql = repos.brands
        .createQueryBuilder("b")
        .distinct(true)
        .innerJoin("b.products", "products")
        .innerJoin(
          "products.categoryProduct",
          "categoryProduct",
          "categoryProduct.is_active = true"
        )
        .innerJoin(
          Categories,
          "SubCategoryProduct",
          "SubCategoryProduct.category_code = products.category and SubCategoryProduct.is_active = true"
        )
        .leftJoin("products.stockBalance", "stockBalance")
        .where("b.is_active = true")
        .andWhere("COALESCE(products.stock_online, 0) > 0")
        .andWhere("COALESCE(products.stock_online, 0) >= products.min_stock")
        .orderBy("b.sequence", "ASC");

      const brands = await sql.getMany();
      return brands;
    } catch (e) {
      throw e;
    }
  }

  async getPLPDetail(req: any, res: any) {
    try {
      const {
        type,
        code,
        segment,
        customerId,
        lang,
        sortBy,
        orderBy,
        priceMin,
        priceMax,
        ratingStart,
        brandCodes,
        categoryCodes,
        countryCodes,
        isSimilar
      }: IRequestPLPDetail = req.body;

      if (!EShopByTab[type?.toUpperCase()]) {
        return res.status(400).json({
          code: 400,
          message: "Type of shop by is invalid.",
          data: {},
        });
      }

      const customer = customerId
        ? await manager.customer.findByCustomerId(customerId)
        : null;
      const customerSegment = await manager.customer.findSegment(
        customer?.segment_code || ESegment.GUESS
      );

      const condition: IConditionShopByPLP = {
        lang,
        sortBy,
        orderBy,
        priceMin,
        priceMax,
        ratingStart,
        brandCodes,
        categoryCodes,
        countryCodes,
        isSimilar
      };
      let products = [];
      switch (type) {
        case EShopByTab.CATEGORY:
        case EShopByTab.CATEGORIES:
          products = await this.getCategoryPLP(
            code,
            customer,
            customerSegment,
            condition
          );
          break;
        case EShopByTab.COUNTRY:
        case EShopByTab.COUNTRIES:
          products = await this.getCountryPLP(
            code,
            customer,
            customerSegment,
            condition
          );
          break;
        case EShopByTab.BRAND:
        case EShopByTab.BRANDS:
          products = await this.getBrandPLP(
            code,
            customer,
            customerSegment,
            condition
          );
          break;
        default:
      }

      const categories = [];
      if (
        [
          EShopByTab.COUNTRY,
          EShopByTab.COUNTRIES,
          EShopByTab.BRAND,
          EShopByTab.BRANDS,
        ].includes(type)
      ) {
        await Promise.all(
          products.map((product) => {
            if (!categories.find((cat) => cat.cat_code === product.cat_code)) {
              categories.push({
                cat_code: product.cat_code,
                group_name_th: product.group_name_th,
                group_name_en: product.group_name_en,
                total_items: products.filter(
                  (e) => e.cat_code === product.cat_code
                ).length,
              });
            }
          })
        );
      } else {
        await Promise.all(
          products.map((product) => {
            if (!categories.find((cat) => cat.category === product.category)) {
              categories.push({
                category: product.category,
                sub_category_th: product.sub_category_th,
                sub_category_en: product.sub_category_en,
                total_items: products.filter(
                  (e) => e.category === product.category
                ).length,
              });
            }
          })
        );
      }

      console.log('products', products.length)
      return {
        products: products,
        categories: categories,
      };
    } catch (e) {
      throw e;
    }
  }

  async getCategoryPLP(
    code: string,
    customer: Customer,
    customerSegment: CustomerSegment,
    condition: IConditionShopByPLP
  ) {
    try {
      const repos = global.repos;
      let sql = repos.mappingProducts
        .createQueryBuilder("mp")
        .innerJoinAndSelect("mp.categoryProduct", "cp")
        .leftJoin("mp.countryProduct", "c")
        .leftJoin("mp.brandProduct", "b")
        .leftJoinAndSelect("mp.productDiscount", "pd")
        .leftJoinAndSelect("mp.badgeBuyDisc", "bbd")
        .leftJoinAndSelect("mp.badgeBuyFree", "bbf")

      if (customer) {
        sql = sql.leftJoinAndSelect(
          "mp.productFavorite",
          "pf",
          "pf.customer_id = :customerId",
          {
            customerId: customer.customer_id,
          }
        );
      }

      sql = await this.getCondition(sql, condition);
      if (code) {
        sql = sql.andWhere("mp.cat_code = :code", {
          code: code,
        });
      }

      sql = sql
        .orderBy("mp.product_created_date", "ASC")
        .addOrderBy("mp.id", "ASC");
      let datas = await sql.getMany();
      datas = await this.formatData(datas, customerSegment, condition);
      return datas;
    } catch (e) {
      throw e;
    }
  }

  async getALlProductDetails(
    customer: Customer,
    customerSegment: CustomerSegment,
    sku: String[]
  ) {
    try {
      let code = "";
      const condition: IConditionShopByPLP = {
        lang: null,
        sortBy: null,
        orderBy: null,
        priceMin: null,
        priceMax: null,
        ratingStart: null,
        brandCodes: null,
        categoryCodes: null,
        countryCodes: null,
      };
      const repos = global.repos;
      let sql = repos.mappingProducts
        .createQueryBuilder("mp")
        .innerJoinAndSelect("mp.categoryProduct", "cp")
        .leftJoin("mp.countryProduct", "c")
        .leftJoin("mp.brandProduct", "b")
        .leftJoinAndSelect("mp.productDiscount", "pd")
        .leftJoinAndSelect("mp.badgeBuyDisc", "bbd")
        .leftJoinAndSelect("mp.badgeBuyFree", "bbf")

      if (customer) {
        sql = sql.leftJoinAndSelect(
          "mp.productFavorite",
          "pf",
          "pf.customer_id = :customerId",
          {
            customerId: customer.customer_id,
          }
        );
      }

      sql = await this.getCondition(sql, condition);
      sql = sql.andWhere("mp.sku IN (:...sku)", {
        sku: sku,
      });

      sql = sql
        .orderBy("mp.product_created_date", "ASC")
        .addOrderBy("mp.id", "ASC");
      sql = sql.orderBy("mp.pr_name_en", "ASC");
      let datas = await sql.getMany();
      datas = await this.formatData(datas, customerSegment, condition);
      return datas;
    } catch (e) {
      throw e;
    }
  }

  async getCountryPLP(
    code: string,
    customer: Customer,
    customerSegment: CustomerSegment,
    condition: IConditionShopByPLP
  ) {
    try {
      const repos = global.repos;
      let sql = repos.mappingProducts
        .createQueryBuilder("mp")
        .innerJoinAndSelect("mp.categoryProduct", "cp")
        .innerJoin("mp.countryProduct", "c")
        .leftJoin("mp.brandProduct", "b")
        .leftJoinAndSelect("mp.productDiscount", "pd")
        .leftJoinAndSelect("mp.badgeBuyDisc", "bbd")
        .leftJoinAndSelect("mp.badgeBuyFree", "bbf")

      if (customer) {
        sql = sql.leftJoinAndSelect(
          "mp.productFavorite",
          "pf",
          "pf.customer_id = :customerId",
          {
            customerId: customer.customer_id,
          }
        );
      }

      sql = await this.getCondition(sql, condition);
      if (code) {
        sql = sql.andWhere("c.country_code = :code", {
          code: code,
        });
      }

      sql = sql
        .orderBy("mp.product_created_date", "ASC")
        .addOrderBy("mp.id", "ASC");
      let datas = await sql.getMany();
      datas = await this.formatData(datas, customerSegment, condition);
      return datas;
    } catch (e) {
      throw e;
    }
  }

  async getBrandPLP(
    code: string,
    customer: Customer,
    customerSegment: CustomerSegment,
    condition: IConditionShopByPLP
  ) {
    try {
      const repos = global.repos;
      let sql = repos.mappingProducts
        .createQueryBuilder("mp")
        .innerJoinAndSelect("mp.categoryProduct", "cp")
        .innerJoin("mp.brandProduct", "b")
        .leftJoin("mp.countryProduct", "c")
        .leftJoinAndSelect("mp.productDiscount", "pd")
        .leftJoinAndSelect("mp.badgeBuyDisc", "bbd")
        .leftJoinAndSelect("mp.badgeBuyFree", "bbf")
        .leftJoin("mp.stockBalance", "stockBalance");

      if (customer) {
        sql = sql.leftJoinAndSelect(
          "mp.productFavorite",
          "pf",
          "pf.customer_id = :customerId",
          {
            customerId: customer.customer_id,
          }
        );
      }

      sql = await this.getCondition(sql, condition);
      if (code) {
        sql = sql.andWhere("b.brand_code = :code", {
          code: code,
        });
      }

      sql = sql
        .orderBy("mp.product_created_date", "ASC")
        .addOrderBy("mp.id", "ASC");
      let datas = await sql.getMany();
      datas = await this.formatData(datas, customerSegment, condition);
      return datas;
    } catch (e) {
      throw e;
    }
  }

  async getCondition(sql: any, condition: IConditionShopByPLP) {
    try {
      sql = sql.andWhere("COALESCE(mp.stock_online, 0) > 0");
      sql = sql.andWhere("COALESCE(mp.stock_online, 0) >= mp.min_stock");

      if (condition.isSimilar) {
        sql = sql.andWhere("COALESCE(mp.stock_online, 0) > mp.safety_stock");
      }
      
      if (
        condition?.categoryCodes?.length > 0 ||
        condition?.brandCodes?.length > 0 ||
        condition?.countryCodes?.length > 0
      ) {
        sql = sql.andWhere(
          new Brackets((qb) => {
            if (condition?.categoryCodes?.length > 0) {
              qb = qb.orWhere("mp.category in (:...categoryCodes)", {
                categoryCodes: condition.categoryCodes,
              });
            }

            if (condition?.brandCodes?.length > 0) {
              qb = qb.orWhere("b.brand_code in (:...brandCodes)", {
                brandCodes: condition.brandCodes,
              });
            }

            if (condition?.countryCodes?.length > 0) {
              qb = qb.orWhere("c.country_code in (:...countryCodes)", {
                countryCodes: condition.countryCodes,
              });
            }
          })
        );
      }

      if (condition?.ratingStart) {
        sql = sql.andWhere("mp.product_rating >= :ratingStart", {
          ratingStart: condition.ratingStart,
        });
      }

      return sql;
    } catch (e) {
      throw e;
    }
  }

  async formatData(
    products,
    customerSegment: CustomerSegment,
    condition: IConditionShopByPLP
  ) {
    try {
      let result = await Promise.all(
        products.map(async (product) => {
          await Promise.all(
            Object.keys(product).map((key) => {
              if (
                [
                  "price_normal",
                  "member1_price",
                  "member2_price",
                  "member3_price",
                  "member4_price",
                  "member5_price",
                  "product_rating",
                  "min_stock",
                  "safety_stock",
                  "limit_per_order",
                  "stock_online",
                ].includes(key)
              ) {
                product[key] = isNaN(product[key])
                  ? product[key]
                  : Number(product[key]);
              }
            })
          );
          const priceDiscount = await manager.product.calculatePriceDiscount(
            customerSegment,
            product.pr_code,
            product.price_normal,
          );
          product.price_discount = priceDiscount || 0;
          product.price_net = Math.ceil(
            product.price_normal - product.price_discount
          );
          product.customer_badge_en = customerSegment?.customer_badge_en;
          product.customer_badge_th = customerSegment?.customer_badge_th;
          product.is_favorite = product?.productFavorite
            ? product.productFavorite.is_favorite
            : false;
          product.is_similar_stock =
            (product.stock_online || 0) >= product.min_stock &&
            (product.stock_online || 0) <= product.safety_stock;
          product.is_redhot = product?.badgeBuyDisc ? 1 : 0;
          product.is_freegift = product?.badgeBuyFree ? 1 : 0;

          delete product?.categoryProduct;
          delete product?.productDiscount;
          delete product?.productFavorite;
          return product;
        })
      );

      if (condition?.priceMax && condition?.priceMin) {
        result = result.filter(
          (item) =>
            item.price_net >= condition.priceMin &&
            item.price_net <= condition.priceMax
        );
      } else if (condition?.priceMax) {
        result = result.filter((item) => item.price_net <= condition.priceMax);
      } else if (condition?.priceMin) {
        result = result.filter((item) => item.price_net >= condition.priceMin);
      }

      if (condition?.sortBy) {
        switch (condition?.sortBy) {
          case ESortByType.RELEVANCE:
            break;
          case ESortByType.NEW_ARRIVALS:
            result = result.sort((a, b) =>
              condition?.orderBy?.toUpperCase() === EOrderBy.ASC
                ? a.product_created_date - b.product_created_date
                : b.product_created_date - a.product_created_date
            );
            break;
          case ESortByType.RATINGS:
            result = result.sort((a, b) =>
              condition?.orderBy?.toUpperCase() === EOrderBy.ASC
                ? a.product_rating - b.product_rating
                : b.product_rating - a.product_rating
            );
            break;
          case ESortByType.PRICE:
            result = result.sort((a, b) =>
              condition?.orderBy?.toUpperCase() === EOrderBy.ASC
                ? a.price_net - b.price_net
                : b.price_net - a.price_net
            );
            break;
          case ESortByType.NAME:
            if (condition?.lang?.toUpperCase() === ELanguage.TH) {
              result = result.sort((a, b) =>
                condition?.orderBy?.toUpperCase() === EOrderBy.ASC
                  ? a.pr_name_th.localeCompare(b.pr_name_th)
                  : b.pr_name_th.localeCompare(a.pr_name_th)
              );
            } else {
              result = result.sort((a, b) =>
                condition?.orderBy?.toUpperCase() === EOrderBy.ASC
                  ? a.pr_name_en.localeCompare(b.pr_name_en)
                  : b.pr_name_en.localeCompare(a.pr_name_en)
              );
            }
            break;
          default:
        }
      }

      return result;
    } catch (e) {
      throw e;
    }
  }

  async getFilterLists(
    lang: string,
    type?: EShopByTab,
    code?: string,
    branchId?: string,
    isSimilar?: boolean
  ) {
    try {
      let category: CategoryFilterList[] = null;
      if (![EShopByTab.CATEGORIES, EShopByTab.CATEGORY].includes(type)) {
        category = await this.getCategoryFilters(lang, type, code, branchId, isSimilar);
      }

      let country: CountryFilterList[] = null;
      if (![EShopByTab.COUNTRIES, EShopByTab.COUNTRY].includes(type)) {
        country = await this.getCountryFilters(lang, type, code, branchId, isSimilar);
      }

      let brand: BrandFilterList = null;
      if (![EShopByTab.BRANDS, EShopByTab.BRAND].includes(type)) {
        brand = await this.getBrandFilters(lang, type, code, branchId, isSimilar);
      }
      return {
        categories: category,
        countries: country,
        brands: brand,
      };
    } catch (e) {
      throw e;
    }
  }

  async getCategoryFilters(
    lang: string,
    type?: EShopByTab,
    code?: string,
    branchId?: string,
    isSimilar?: boolean
  ): Promise<CategoryFilterList[]> {
    try {
      const repos = global.repos;
      let catLv1Sql = repos.categories
        .createQueryBuilder("cat")
        .select([
          "cat.category_code as category_code",
          "cat.category_code_user as category_code_user",
          "cat.category_name_th as category_name_th",
          "cat.category_name_en as category_name_en",
          "cat.category_image_th as category_image_th",
          "cat.category_image_en as category_image_en",
          "cat.category_icon as category_icon",
          "cat.category_icon_th as category_icon_th",
          "cat.category_icon_en as category_icon_en",
          "cat.sequence as sequence",
        ])
        .leftJoin("cat.products", "products")
        .leftJoin(
          Categories,
          "SubCategoryProduct",
          "SubCategoryProduct.category_code = products.category and SubCategoryProduct.is_active = true"
        )
        .where("cat.category_root_id is null and cat.is_active = true")
        .andWhere("COALESCE(products.stock_online, 0) > 0")
        .andWhere("COALESCE(products.stock_online, 0) >= products.min_stock");
        
      if (isSimilar) {
        catLv1Sql = catLv1Sql.andWhere("COALESCE(products.stock_online, 0) > products.safety_stock");
      }
      
      switch (type) {
        case EShopByTab.BRANDS:
        case EShopByTab.BRAND:
          catLv1Sql = catLv1Sql.andWhere("products.brand = :brandCode", {
            brandCode: code,
          });
          break;
        case EShopByTab.COUNTRIES:
        case EShopByTab.COUNTRY:
          catLv1Sql = catLv1Sql.andWhere(
            "products.country_code = :countryCode",
            {
              countryCode: code,
            }
          );
          break;
        default:
      }

      catLv1Sql = catLv1Sql
        .groupBy("cat.category_code")
        .addGroupBy("cat.category_code_user")
        .addGroupBy("cat.category_name_th")
        .addGroupBy("cat.category_name_en")
        .addGroupBy("cat.category_image_th")
        .addGroupBy("cat.category_image_en")
        .addGroupBy("cat.category_icon")
        .addGroupBy("cat.category_icon_th")
        .addGroupBy("cat.category_icon_en")
        .addGroupBy("cat.sequence")
        .orderBy("cat.sequence", "ASC");

      const catLv1Lists = await catLv1Sql.getRawMany();
      const result = await Promise.all(
        catLv1Lists.map(async (catLv1) => {
          let subCatSql = repos.categories
            .createQueryBuilder("cat")
            .select([
              "cat.category_code as category_code",
              "cat.category_code_user as category_code_user",
              "cat.category_name_th as category_name_th",
              "cat.category_name_en as category_name_en",
              "cat.category_image_th as category_image_th",
              "cat.category_image_en as category_image_en",
              "cat.category_icon as category_icon",
              "cat.category_icon_th as category_icon_th",
              "cat.category_icon_en as category_icon_en",
              "cat.sequence as sequence",
              "count(mp.*) as total_products",
            ])
            .leftJoin(MappingProducts, "mp", "mp.category = cat.category_code")
            .where("mp.cat_code = :catCodeLv1 and cat.is_active = true", {
              catCodeLv1: catLv1.category_code,
            })
            .andWhere("COALESCE(mp.stock_online, 0) > 0")
            .andWhere("COALESCE(mp.stock_online, 0) >= mp.min_stock");

          if (isSimilar) {
            subCatSql = subCatSql.andWhere("COALESCE(mp.stock_online, 0) > mp.safety_stock");
          }

          switch (type) {
            case EShopByTab.BRANDS:
            case EShopByTab.BRAND:
              subCatSql = subCatSql.andWhere("mp.brand = :brandCode", {
                brandCode: code,
              });
              break;
            case EShopByTab.COUNTRIES:
            case EShopByTab.COUNTRY:
              subCatSql = subCatSql.andWhere("mp.country_code = :countryCode", {
                countryCode: code,
              });
              break;
            default:
          }

          subCatSql = subCatSql
            .groupBy("cat.category_code")
            .addGroupBy("cat.category_code_user")
            .addGroupBy("cat.category_name_th")
            .addGroupBy("cat.category_name_en")
            .addGroupBy("cat.category_image_th")
            .addGroupBy("cat.category_image_en")
            .addGroupBy("cat.category_icon")
            .addGroupBy("cat.category_icon_th")
            .addGroupBy("cat.category_icon_en")
            .addGroupBy("cat.sequence")
            .orderBy("cat.sequence", "ASC");

          const subCategories = await subCatSql.getRawMany();
          catLv1.sub_category = (subCategories || []).map((subCat) => ({
            ...subCat,
            total_products: Number(subCat.total_products),
          }));
          catLv1.total_products = catLv1.sub_category.reduce(
            (prev, next) => prev + next.total_products,
            0
          );
          return catLv1;
        })
      );
      return result.filter((cat) => cat.sub_category.length > 0);
    } catch (e) {
      throw e;
    }
  }

  async getCountryFilters(
    lang: string,
    type?: EShopByTab,
    code?: string,
    branchId?: string,
    isSimilar?: boolean
  ): Promise<CountryFilterList[]> {
    try {
      const repos = global.repos;
      let sql = repos.countries
        .createQueryBuilder("c")
        .select([
          "c.country_code as country_code",
          "c.country_name_th as country_name_th",
          "c.country_name_en as country_name_en",
          "c.country_image_th as country_image_th",
          "c.country_image_en as country_image_en",
          "c.country_logo as country_logo",
          "c.country_banner_th as country_banner_th",
          "c.country_banner_en as country_banner_en",
          "c.sequence as sequence",
          "count(products.*) as total_products",
        ])
        .leftJoin("c.products", "products")
        // .innerJoin('products.categoryProduct', 'categoryProduct', 'categoryProduct.is_active = true')
        // .innerJoin(Categories, 'SubCategoryProduct', 'SubCategoryProduct.category_code = products.category and SubCategoryProduct.is_active = true')
        .where("c.is_active = true")
        .andWhere("COALESCE(products.stock_online, 0) > 0")
        .andWhere("COALESCE(products.stock_online, 0) >= products.min_stock");

      if (isSimilar) {
        sql = sql.andWhere("COALESCE(products.stock_online, 0) > products.safety_stock");
      }

      switch (type) {
        case EShopByTab.CATEGORIES:
        case EShopByTab.CATEGORY:
          sql = sql.andWhere("(products.cat_code = :categoryCode or products.category = :categoryCode)", {
            categoryCode: code,
          });
          break;
        case EShopByTab.BRANDS:
        case EShopByTab.BRAND:
          sql = sql.andWhere("products.brand = :brandCode", {
            brandCode: code,
          });
          break;
        default:
      }

      sql = sql
        .groupBy("c.country_code")
        .addGroupBy("c.country_name_th")
        .addGroupBy("c.country_name_en")
        .addGroupBy("c.country_image_th")
        .addGroupBy("c.country_image_en")
        .addGroupBy("c.country_logo")
        .addGroupBy("c.country_banner_th")
        .addGroupBy("c.country_banner_en")
        .addGroupBy("c.sequence")
        .orderBy("c.sequence", "ASC");

      const countries = await sql.getRawMany();
      return await countries.map((country) => ({
        ...country,
        total_products: Number(country.total_products),
      }));
    } catch (e) {
      throw e;
    }
  }

  async getBrandFilters(
    lang: string,
    type?: EShopByTab,
    code?: string,
    branchId?: string,
    isSimilar?: boolean
  ): Promise<BrandFilterList> {
    try {
      const repos = global.repos;
      let sql = repos.brands
        .createQueryBuilder("b")
        .select([
          "b.brand_code as brand_code",
          "b.brand_name_th as brand_name_th",
          "b.brand_name_en as brand_name_en",
          "b.brand_image_th as brand_image_th",
          "b.brand_image_en as brand_image_en",
          "b.is_popular as is_popular",
          "b.sequence as sequence",
          "count(products.*) as total_products",
        ])
        .leftJoin("b.products", "products")
        // .innerJoin('products.categoryProduct', 'categoryProduct', 'categoryProduct.is_active = true')
        // .innerJoin(Categories, 'SubCategoryProduct', 'SubCategoryProduct.category_code = products.category and SubCategoryProduct.is_active = true')
        .where("b.is_active = true")
        .andWhere("COALESCE(products.stock_online, 0) > 0")
        .andWhere("COALESCE(products.stock_online, 0) >= products.min_stock");

      if (isSimilar) {
        sql = sql.andWhere("COALESCE(products.stock_online, 0) > products.safety_stock");
      }

      switch (type) {
        case EShopByTab.CATEGORIES:
        case EShopByTab.CATEGORY:
          sql = sql.andWhere("(products.cat_code = :categoryCode or products.category = :categoryCode)", {
            categoryCode: code,
          });
          break;
        case EShopByTab.COUNTRIES:
        case EShopByTab.COUNTRY:
          sql = sql.andWhere("products.country_code = :countryCode", {
            countryCode: code,
          });
          break;
        default:
      }

      sql = sql
        .groupBy("b.brand_code")
        .addGroupBy("b.brand_name_th")
        .addGroupBy("b.brand_name_en")
        .addGroupBy("b.brand_image_th")
        .addGroupBy("b.brand_image_en")
        .addGroupBy("b.is_popular")
        .addGroupBy("b.sequence")
        .orderBy("b.sequence", "ASC");

      const brands = await sql.getRawMany();

      const popular: BrandInfoFilterList[] = brands
        .filter((brand) => brand.is_popular === true)
        .map((brand) => ({
          ...brand,
          total_products: Number(brand.total_products),
        }));
      const nonPopular: BrandInfoFilterList[] = brands
        .map((brand) => ({
          ...brand,
          total_products: Number(brand.total_products),
        }));
      return {
        popular,
        nonPopular,
      };
    } catch (e) {
      throw e;
    }
  }
}
