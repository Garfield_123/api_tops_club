import { manager } from ".";
import { ELanguage } from "../types";

export class SectionManager {
    async getContentDefault (section: any, customer: any, customerSegment: any): Promise<any> {
        try {
            const rawSql = global.rawSql;
            const contents: any = await rawSql.query(`
                select  case when mapping_products.id is not null then mapping_products.id else contents.id end as id,
                        contents.sequence,
                        contents.is_active as content_active,
                        image.url,
                        image.image_en,
                        categories.category_code AS t_category_code,
                        categories.category_name_th AS t_category_name_th,
                        categories.category_name_en AS t_category_name_en,
                        categories.category_image_th AS t_category_image_th,
                        categories.category_image_en AS t_category_image_en,
                        categories.category_icon AS t_category_icon,
                        categories.category_icon_th AS t_category_icon_th,
                        categories.category_icon_en AS t_category_icon_en,
                        contents.section_type_key,
                        countries.country_code AS t_country_code,
                        countries.country_name_th AS t_country_name_th,
                        countries.country_name_en AS t_country_name_en,
                        countries.country_image_th AS t_country_image_th,
                        countries.country_image_en AS t_country_image_en,
                        countries.country_short AS t_country_short,
                        countries.country_logo AS t_country_logo,
                        countries.country_banner_th AS t_country_banner_th,
                        countries.country_banner_en AS t_country_banner_en,
                        mapping_products.group_name_en,
                        mapping_products.group_name_th,
                        mapping_products.pr_code,
                        mapping_products.pr_name_th,
                        mapping_products.pr_name_en,
                        mapping_products.price_normal,
                        mapping_products.member1_price,
                        mapping_products.brand,
                        mapping_products.brand_name_th,
                        mapping_products.brand_name_en,
                        mapping_products.brand_image_th,
                        mapping_products.brand_image_en,
                        mapping_products.is_popular,
                        mapping_products.country_code,
                        mapping_products.country_name_th,
                        mapping_products.country_name_en,
                        mapping_products.country_image_th,
                        mapping_products.country_image_en,
                        mapping_products.country_image_icon,
                        mapping_products.cat_code,
                        mapping_products.category,
                        mapping_products.sub_category,
                        mapping_products.sub_category_th,
                        mapping_products.sub_category_en,
                        mapping_products.thumbnail,
                        mapping_products.image,
                        mapping_products.upselling_badge_th,
                        mapping_products.upselling_badge_en,
                        mapping_products.promotion_badge_th,
                        mapping_products.promotion_badge_en,
                        mapping_products.service_badge,
                        mapping_products.stock_on_hand,
                        mapping_products.stock_online,
                        mapping_products.product_image_list,
                        mapping_products.product_status,
                        mapping_products.product_type,
                        mapping_products.is_active as product_active,
                        mapping_products.consumer_unit_th,
                        mapping_products.consumer_unit_en,
                        mapping_products.products_badge_th,
                        mapping_products.products_badge_en,
                        mapping_products.promotion_discount,
                        mapping_products.product_rating,
                        mapping_products.short_description_th,
                        mapping_products.short_description_en,
                        mapping_products.long_description_th,
                        mapping_products.long_description_en,
                        mapping_products.min_stock,
                        mapping_products.safety_stock,
                        mapping_products.limit_per_order,
                        brands.brand_code AS t_brand_code,
                        brands.brand_name_th AS t_brand_name_th,
                        brands.brand_name_en AS t_brand_name_en,
                        brands.brand_image_th AS t_brand_image_th,
                        brands.brand_image_en AS t_brand_image_en,
                        categories.percent_discount AS category_percent_discount,
                        product_discount.percent_discount AS product_percent_discount,
                        product_favorite.is_favorite as is_favorite
                from contents
                left join countries ON contents.section_type_key = countries.country_code
                left join brands ON contents.section_type_key = brands.brand_code
                left join image ON contents.section_type_key = image.cms_key
                left join categories ON contents.section_type_key = categories.category_code
                left join mapping_products ON contents.section_type_key = mapping_products.pr_code
                left join product_discount on product_discount.pr_code = mapping_products.pr_code
                left join product_favorite on product_favorite.pr_code = mapping_products.pr_code and product_favorite.customer_id = $1
                where contents.section_id = $2
                AND contents.is_active = TRUE
                Order by contents.sequence ASC
            `, [
                customer?.customer_id || 0,
                section.section_id,
            ])

            section.contents = await this.setContents(section.section_type, contents, customerSegment);
            return section;
        } catch (e) {
            throw e;
        }
    }

    async getContentCategory (section: any, customerSegment: any): Promise<any> {
        try {
            const rawSql = global.rawSql;
            const contents: any = await rawSql.query(`
                select distinct 
                        categories.category_code AS t_category_code,
                        categories.category_name_th AS t_category_name_th,
                        categories.category_name_en AS t_category_name_en,
                        categories.category_image_th AS t_category_image_th,
                        categories.category_image_en AS t_category_image_en,
                        categories.category_icon AS t_category_icon,
                        categories.category_icon_th AS t_category_icon_th,
                        categories.category_icon_en AS t_category_icon_en,
                        categories.sequence AS sequence
                from categories
                inner join mapping_products ON categories.category_code = mapping_products.cat_code
                inner join categories as subCategories on subCategories.category_code = mapping_products.category and subCategories.is_active = true
                where categories.is_active = true
                AND COALESCE(mapping_products.stock_online, 0) > 0
                AND COALESCE(mapping_products.stock_online, 0) >= mapping_products.min_stock
                Order by categories.sequence ASC`)

            section.contents = await this.setContents(section.section_type, contents, customerSegment);
            return section;
        } catch (e) {
            throw e;
        }
    }

    async getContentCountry (lang: string, section: any, customerSegment: any): Promise<any> {
        try {
            const rawSql = global.rawSql;
            const contents: any = await rawSql.query(`
                select distinct 
                        countries.country_code AS t_country_code,
                        countries.country_name_th AS t_country_name_th,
                        countries.country_name_en AS t_country_name_en,
                        countries.country_image_th AS t_country_image_th,
                        countries.country_image_en AS t_country_image_en,
                        countries.country_short AS t_country_short,
                        countries.country_logo AS t_country_logo,
                        countries.country_banner_th AS t_country_banner_th,
                        countries.country_banner_en AS t_country_banner_en,
                        countries.sequence AS sequence
                from countries
                inner join mapping_products ON countries.country_code = mapping_products.country_code
                inner join categories on categories.category_code = mapping_products.cat_code and categories.is_active = true
                inner join categories as subCategories on subCategories.category_code = mapping_products.category and subCategories.is_active = true
                where countries.is_active = true
                AND COALESCE(mapping_products.stock_online, 0) > 0
                AND COALESCE(mapping_products.stock_online, 0) >= mapping_products.min_stock
                Order by countries.sequence ASC`)

            section.contents = await this.setContents(section.section_type, contents, customerSegment);
            return section;
        } catch (e) {
            throw e;
        }
    }

    async getContentBrand (lang: string, section: any, customerSegment: any): Promise<any> {
        try {
            const rawSql = global.rawSql;
            const contents: any = await rawSql.query(`
                select distinct 
                        brands.brand_code AS t_brand_code,
                        brands.brand_name_th AS t_brand_name_th,
                        brands.brand_name_en AS t_brand_name_en,
                        brands.brand_image_th AS t_brand_image_th,
                        brands.brand_image_en AS t_brand_image_en,
                        brands.sequence AS sequence
                from brands
                inner join mapping_products ON brands.brand_code = mapping_products.brand
                inner join categories on categories.category_code = mapping_products.cat_code and categories.is_active = true
                inner join categories as subCategories on subCategories.category_code = mapping_products.category and subCategories.is_active = true
                where brands.is_active = true
                AND COALESCE(mapping_products.stock_online, 0) > 0
                AND COALESCE(mapping_products.stock_online, 0) >= mapping_products.min_stock
                Order by brands.sequence ASC`)

            section.contents = await this.setContents(section.section_type, contents, customerSegment);
            return section;
        } catch (e) {
            throw e;
        }
    }

    async getContentProduct (section: any, customer: any, customerSegment: any, isPLP?: boolean): Promise<any> {
        try {
            const rawSql = global.rawSql;
            const contents: any = await rawSql.query(`
                select  case when mapping_products.id is not null then mapping_products.id else contents.id end as id,
                        contents.sequence,
                        contents.is_active as content_active,
                        categories.category_code AS t_category_code,
                        categories.category_name_th AS t_category_name_th,
                        categories.category_name_en AS t_category_name_en,
                        categories.category_image_th AS t_category_image_th,
                        categories.category_image_en AS t_category_image_en,
                        categories.category_icon AS t_category_icon,
                        categories.category_icon_th AS t_category_icon_th,
                        categories.category_icon_en AS t_category_icon_en,
                        contents.section_type_key,
                        countries.country_code AS t_country_code,
                        countries.country_name_th AS t_country_name_th,
                        countries.country_name_en AS t_country_name_en,
                        countries.country_image_th AS t_country_image_th,
                        countries.country_image_en AS t_country_image_en,
                        countries.country_short AS t_country_short,
                        countries.country_logo AS t_country_logo,
                        countries.country_banner_th AS t_country_banner_th,
                        countries.country_banner_en AS t_country_banner_en,
                        mapping_products.group_name_en,
                        mapping_products.group_name_th,
                        mapping_products.pr_code,
                        mapping_products.pr_name_th,
                        mapping_products.pr_name_en,
                        mapping_products.price_normal,
                        mapping_products.member1_price,
                        mapping_products.brand,
                        mapping_products.brand_name_th,
                        mapping_products.brand_name_en,
                        mapping_products.brand_image_th,
                        mapping_products.brand_image_en,
                        mapping_products.is_popular,
                        mapping_products.country_code,
                        mapping_products.country_name_th,
                        mapping_products.country_name_en,
                        mapping_products.country_image_th,
                        mapping_products.country_image_en,
                        mapping_products.country_image_icon,
                        mapping_products.cat_code,
                        mapping_products.category,
                        mapping_products.sub_category,
                        mapping_products.sub_category_th,
                        mapping_products.sub_category_en,
                        mapping_products.thumbnail,
                        mapping_products.image,
                        mapping_products.upselling_badge_th,
                        mapping_products.upselling_badge_en,
                        mapping_products.service_badge,
                        mapping_products.stock_on_hand,
                        mapping_products.stock_online,
                        mapping_products.product_image_list,
                        mapping_products.product_status,
                        mapping_products.product_type,
                        mapping_products.sku,
                        mapping_products.is_active as product_active,
                        mapping_products.consumer_unit_th,
                        mapping_products.consumer_unit_en,
                        mapping_products.products_badge_th,
                        mapping_products.products_badge_en,
                        mapping_products.promotion_discount,
                        mapping_products.promotion_badge_th,
                        mapping_products.promotion_badge_en,
                        mapping_products.product_rating,
                        mapping_products.short_description_th,
                        mapping_products.short_description_en,
                        mapping_products.long_description_th,
                        mapping_products.long_description_en,
                        mapping_products.min_stock,
                        mapping_products.safety_stock,
                        mapping_products.limit_per_order,
                        brands.brand_code AS t_brand_code,
                        brands.brand_name_th AS t_brand_name_th,
                        brands.brand_name_en AS t_brand_name_en,
                        brands.brand_image_th AS t_brand_image_th,
                        brands.brand_image_en AS t_brand_image_en,
                        categories.percent_discount AS category_percent_discount,
                        product_discount.percent_discount AS product_percent_discount,
                        product_favorite.is_favorite as is_favorite,
                        case when COALESCE(mapping_products.stock_online, 0) >= mapping_products.min_stock
                                and COALESCE(mapping_products.stock_online, 0) <= mapping_products.safety_stock
                            then true else false end as is_similar_stock,
                        case when bbd.pr_code is not null then true else false end as is_redhot,
                        case when bbf.pr_code is not null then true else false end as is_freegift
                from contents
                left join mapping_products ON contents.section_type_key = mapping_products.pr_code
                inner join categories on categories.category_code = mapping_products.cat_code and categories.is_active = true
                inner join categories as subCategories on subCategories.category_code = mapping_products.category and subCategories.is_active = true
                left join product_discount on product_discount.pr_code = mapping_products.pr_code
                left join product_favorite on product_favorite.pr_code = mapping_products.pr_code and product_favorite.customer_id = $1
                left join brands ON mapping_products.brand = brands.brand_code
                left join countries ON mapping_products.country_code = countries.country_code
                left join badge_buy_disc bbd on bbd.pr_code = mapping_products.pr_code
                left join badge_buy_free bbf on bbf.pr_code = mapping_products.pr_code
                where contents.section_id = $2
                AND contents.is_active = TRUE
                AND COALESCE(mapping_products.stock_online, 0) > 0
                AND COALESCE(mapping_products.stock_online, 0) >= mapping_products.min_stock
                Order by contents.sequence ASC
                ${!isPLP ? 'LIMIT 4' : ''}`, [//fix 4 ที่เหลือกด view all in plp 
                    customer?.customer_id || 0,
                    section.section_id
                ])

            section.contents = await this.setContents(section.section_type, contents, customerSegment);
            return section;
        } catch (e) {
            throw e;
        }
    }

    async getContentImage (section: any, customer: any, customerSegment: any): Promise<any> {
        try {
            const rawSql = global.rawSql;
            const contents: any = await rawSql.query(`
                select  contents.id,
                        contents.sequence,
                        contents.is_active as content_active,
                        image.url,
                        image.image_en,
                        contents_type.type_name as contents_type_key,
                        contents.link as contents_link,
                        contents.key as contents_type_value
                from contents
                left join image ON contents.section_type_key = image.cms_key
                left join contents_type ON contents.type = contents_type.id
                where contents.section_id = $1 AND contents.is_active = TRUE
                Order by contents.sequence ASC`, [
                    section.section_id,
                ])

            section.contents = await this.setContents(section.section_type, contents, customerSegment);
            return section;
        } catch (e) {
            throw e;
        }
    }

    async setContents (type: string, contents: any, customerSegment: any) {
        try {
            return await Promise.all(contents.map(async content => {
                const priceNormal = Number(content.price_normal || 0);
                const priceDiscount = await manager.product.calculatePriceDiscount(customerSegment, content?.pr_code, priceNormal);
                const priceNet = Math.ceil(priceNormal - priceDiscount);
                return {
                        id: content?.id || null,
                        sequence: content?.sequence || null,
                        is_active: content?.content_active || null,
                        image_th: content?.url || null,
                        image_en: content?.image_en || null,
                        contents_type_key: content?.contents_type_key || null,
                        contents_type_value: content?.contents_type_value || null,
                        contents_link: content?.contents_link || null,
                        t_category_code: content?.t_category_code || null,
                        t_category_name_th: content?.t_category_name_th || null,
                        t_category_name_en: content?.t_category_name_en || null,
                        t_category_image_th: content?.t_category_image_th || null,
                        t_category_image_en: content?.t_category_image_en || null,
                        t_category_icon: content?.t_category_icon || null,
                        t_category_icon_th: content?.t_category_icon_th || null,
                        t_category_icon_en: content?.t_category_icon_en || null,
                        t_country_code: content?.t_country_code || null,
                        t_country_name_th: content?.t_country_name_th || null,
                        t_country_name_en: content?.t_country_name_en || null,
                        t_country_image_th: content?.t_country_image_th || null,
                        t_country_image_en: content?.t_country_image_en || null,
                        t_country_short: content?.t_country_short || null,
                        t_country_logo: content?.t_country_logo || null,
                        t_country_banner_th: content?.t_country_banner_th || null,
                        t_country_banner_en: content?.t_country_banner_en || null,
                        group_name_th: content?.group_name_th || null,
                        group_name_en: content?.group_name_en || null,
                        pr_code: content?.pr_code || null,
                        pr_name_th: content?.pr_name_th || null,
                        pr_name_en: content?.pr_name_en || null,
                        price_normal: priceNormal,
                        price_discount: priceDiscount,
                        price_net: priceNet,
                        member1_price: Number(content?.member1_price || 0),
                        brand: content?.brand || null,
                        brand_name_th: content?.brand_name_th || null,
                        brand_name_en: content?.brand_name_en || null,
                        brand_image_th: content?.brand_image_th || null,
                        brand_image_en: content?.brand_image_en || null,
                        is_popular: content?.is_popular || null,
                        country_code: content?.country_code || null,
                        country_name_th: content?.country_name_th || null,
                        country_name_en: content?.country_name_en || null,
                        country_image_th: content?.country_image_th || null,
                        country_image_en: content?.country_image_en || null,
                        country_image_icon: content?.country_image_icon || null,
                        cat_code: content?.cat_code || null,
                        category: content?.category || null,
                        sub_category: content?.sub_category || null,
                        sub_category_th: content?.sub_category_th || null,
                        sub_category_en: content?.sub_category_en || null,
                        thumbnail: content?.thumbnail || null,
                        image: content?.image || null,
                        upselling_badge_th: content?.upselling_badge_th || null,
                        upselling_badge_en: content?.upselling_badge_en || null,
                        service_badge: content?.service_badge || null,
                        stock_on_hand: content?.stock_on_hand || null,
                        stock_online: content?.stock_online || null,
                        sku: content?.sku || null,
                        product_image_list: content?.product_image_list || null,
                        product_status: content?.product_status || null,
                        product_type: content?.product_type || null,
                        product_active: content?.product_active || null,
                        consumer_unit_th: content?.consumer_unit_th || null,
                        consumer_unit_en: content?.consumer_unit_en || null,
                        products_badge_th: content?.products_badge_th || null,
                        products_badge_en: content?.products_badge_en || null,
                        promotion_discount: content?.promotion_discount || null,
                        promotion_badge_th: content?.promotion_badge_th || null,
                        promotion_badge_en: content?.promotion_badge_en || null,
                        product_rating: content?.product_rating || null,
                        short_description_th: content?.short_description_th || null,
                        short_description_en: content?.short_description_en || null,
                        long_description_th: content?.long_description_th || null,
                        long_description_en: content?.long_description_en || null,
                        min_stock: content?.min_stock || null,
                        safety_stock: content?.safety_stock || null,
                        limit_per_order: content?.limit_per_order || null,
                        t_brand_code: content?.t_brand_code || null,
                        t_brand: content?.t_brand || null,
                        t_brand_name_th: content?.t_brand_name_th || null,
                        t_brand_name_en: content?.t_brand_name_en || null,
                        t_brand_image_th: content?.t_brand_image_th || null,
                        t_brand_image_en: content?.t_brand_image_en || null,
                        type: type,
                        customer_badge_en: customerSegment?.customer_badge_en,
                        customer_badge_th: customerSegment?.customer_badge_th,
                        is_favorite: content?.is_favorite || false,
                        is_similar_stock: content?.is_similar_stock || false,
                        is_redhot: content?.is_redhot || false,
                        is_freegift: content?.is_freegift || false,
                }
            }));
        } catch (e) {
            throw e;
        }
    }
}
