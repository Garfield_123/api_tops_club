import axios from "axios";
import { config } from "../config";
import { EApiLogSystem, EApiLogType } from "../types/apiLog";
import { manager } from ".";

export class BackendManager {
    async calCart (payload: any): Promise<any> {
        const url = `${config.backendUrl}/webcontrol/carts/cal_cart`;
        try {
            const resultLogApi = await manager.apiLog.saveLogRequest({
                apiRoute: url,
                system: EApiLogSystem.WEBCONTROL,
                type: EApiLogType.INFO,
                request: JSON.stringify(payload),
                remark: 'Call cal_cart',
                ref: payload.Orderno,
            })

            const result = await axios.post(url, payload, {
                headers: {
                    Authorization: `Bearer ${config.webControl.token}`
                }
            });

            await manager.apiLog.saveLogResponse({
                response: JSON.stringify(result.data),
                id: resultLogApi?.id
            })
            return result.data;
        } catch (e) {
            await manager.apiLog.saveLogError({
                apiRoute: url,
                system: EApiLogSystem.WEBCONTROL,
                request: JSON.stringify(payload),
                response: e?.response?.data ? JSON.stringify(e.response.data) : e.message,
                remark: 'Error cal_cart',
                ref: payload.Orderno,
            })
            throw e;
        }
    }

    async confirmCart (payload: any): Promise<any> {
        const url = `${config.backendUrl}/webcontrol/carts/confirm_cart`;
        try {
            const resultLogApi = await manager.apiLog.saveLogRequest({
                apiRoute: url,
                system: EApiLogSystem.WEBCONTROL,
                type: EApiLogType.INFO,
                request: JSON.stringify(payload),
                remark: 'Call confirm_cart',
                ref: payload.Orderno,
            })

            const result = await axios.post(url, payload, {
                headers: {
                    Authorization: `Bearer ${config.webControl.token}`
                }
            });

            await manager.apiLog.saveLogResponse({
                response: JSON.stringify(result.data),
                id: resultLogApi?.id
            })
            return result.data;
        } catch (e) {
            await manager.apiLog.saveLogError({
                apiRoute: url,
                system: EApiLogSystem.WEBCONTROL,
                request: JSON.stringify(payload),
                response: e?.response?.data ? JSON.stringify(e.response.data) : e.message,
                remark: 'Error confirm_cart',
                ref: payload.Orderno,
            })
            throw e;
        }
    }

    async genTicket (payload: any): Promise<any> {
        const url = `${config.backendUrl}/webcontrol/carts/gen_ticket`;
        try {
            const resultLogApi = await manager.apiLog.saveLogRequest({
                apiRoute: url,
                system: EApiLogSystem.WEBCONTROL,
                type: EApiLogType.INFO,
                request: JSON.stringify(payload),
                remark: 'Call carts_gen_ticket',
                ref: payload.Orderno,
            })

            const result = await axios.post(url, payload, {
                headers: {
                    Authorization: `Bearer ${config.webControl.token}`
                }
            });

            await manager.apiLog.saveLogResponse({
                response: JSON.stringify(result.data),
                id: resultLogApi?.id
            })
            return result.data;
        } catch (e) {
            await manager.apiLog.saveLogError({
                apiRoute: url,
                system: EApiLogSystem.WEBCONTROL,
                request: JSON.stringify(payload),
                response: e?.response?.data ? JSON.stringify(e.response.data) : e.message,
                remark: 'Error carts_gen_ticket',
                ref: payload.Orderno,
            })
            throw e;
        }
    }

    async recalCart (payload: any): Promise<any> {
        const url = `${config.backendUrl}/webcontrol/carts/recal_cart`;
        try {
            const resultLogApi = await manager.apiLog.saveLogRequest({
                apiRoute: url,
                system: EApiLogSystem.WEBCONTROL,
                type: EApiLogType.INFO,
                request: JSON.stringify(payload),
                remark: 'Call recal_cart',
                ref: payload.Orderno,
            })

            const result = await axios.post(url, payload, {
                headers: {
                    Authorization: `Bearer ${config.webControl.token}`
                }
            });

            await manager.apiLog.saveLogResponse({
                response: JSON.stringify(result.data),
                id: resultLogApi?.id
            })
            return result.data;
        } catch (e) {
            await manager.apiLog.saveLogError({
                apiRoute: url,
                system: EApiLogSystem.WEBCONTROL,
                request: JSON.stringify(payload),
                response: e?.response?.data ? JSON.stringify(e.response.data) : e.message,
                remark: 'Error recal_cart',
                ref: payload.Orderno,
            })
            throw e;
        }
    }

    async coverageArea (body: any): Promise<any> {
        const { ref: cartRef, saleSource, stCode, PostalCode: postalCode, latitude, longitude, LangCode: langCode } = body;
        const url = `${config.backendUrl}/sts/api/v1/${langCode}/Area/CoverageArea?saleSource=${saleSource}&stCode=${stCode}&PostalCode=${postalCode}&latitude=${latitude}&longitude=${longitude}&LangCode=${langCode}`;
        try {
            const resultLogApi = await manager.apiLog.saveLogRequest({
                apiRoute: url,
                system: EApiLogSystem.STS,
                type: EApiLogType.INFO,
                request: JSON.stringify(body),
                remark: 'Call coverageArea',
                ref: cartRef,
            })

            const result = await axios.get(url, {
                headers: {
                    Authorization: `Bearer ${config.sts.token}`
                }
            });

            await manager.apiLog.saveLogResponse({
                response: JSON.stringify(result.data),
                id: resultLogApi?.id
            })
            return result.data;
        } catch (e) {
            await manager.apiLog.saveLogError({
                apiRoute: url,
                system: EApiLogSystem.STS,
                request: JSON.stringify(body),
                response: e?.response?.data ? JSON.stringify(e.response.data) : e.message,
                remark: 'Error coverageArea',
                ref: cartRef,
            })
            throw e;
        }
    }

    async masterTimeSlot (cartRef: string, langCode: string, serviceCode: string): Promise<any> {
        const url = `${config.backendUrl}/sts/api/v1/${langCode}/Master/Timeslot?serviceCode=${serviceCode}&LangCode=${langCode}`;
        try {
            const resultLogApi = await manager.apiLog.saveLogRequest({
                apiRoute: url,
                system: EApiLogSystem.STS,
                type: EApiLogType.INFO,
                request: JSON.stringify({
                    langCode: langCode,
                    serviceCode: serviceCode
                }),
                remark: 'Call masterTimeSlot',
                ref: cartRef,
            })

            const result = await axios.get(url, {
                headers: {
                    Authorization: `Bearer ${config.sts.token}`
                }
            });

            await manager.apiLog.saveLogResponse({
                response: JSON.stringify(result.data),
                id: resultLogApi?.id
            })
            return result.data;
        } catch (e) {
            await manager.apiLog.saveLogError({
                apiRoute: url,
                system: EApiLogSystem.STS,
                request: JSON.stringify({
                    langCode: langCode,
                    serviceCode: serviceCode
                }),
                response: e?.response?.data ? JSON.stringify(e.response.data) : e.message,
                remark: 'Error masterTimeSlot',
                ref: cartRef,
            })
            throw e;
        }
    }

    async requestBookingTimeSlot (cartRef: string, langCode: string, body: any): Promise<any> {
        const url = `${config.backendUrl}/sts/api/v1/${langCode}/Booking/RequestTimeSlot`;
        try {
            const resultLogApi = await manager.apiLog.saveLogRequest({
                apiRoute: url,
                system: EApiLogSystem.STS,
                type: EApiLogType.INFO,
                request: JSON.stringify(body),
                remark: 'Call requestTimeSlot',
                ref: cartRef,
            })

            const result = await axios.post(url, body, {
                headers: {
                    Authorization: `Bearer ${config.sts.token}`
                }
            });

            await manager.apiLog.saveLogResponse({
                response: JSON.stringify(result.data),
                id: resultLogApi?.id
            })
            return result.data;
        } catch (e) {
            await manager.apiLog.saveLogError({
                apiRoute: url,
                system: EApiLogSystem.STS,
                request: JSON.stringify(body),
                response: e?.response?.data ? JSON.stringify(e.response.data) : e.message,
                remark: 'Error requestTimeSlot',
                ref: cartRef,
            })
            throw e;
        }
    }

    async couponLists (body: any): Promise<any> {
        const url = `${config.backendUrl}/webcontrol/coupons/list_coupon`;
        try {
            const resultLogApi = await manager.apiLog.saveLogRequest({
                apiRoute: url,
                system: EApiLogSystem.WEBCONTROL,
                type: EApiLogType.INFO,
                request: JSON.stringify(body),
                remark: 'Call couponLists',
            })

            const result = await axios.post(url, body, {
                headers: {
                    Authorization: `Bearer ${config.webControl.token}`
                }
            });

            await manager.apiLog.saveLogResponse({
                response: JSON.stringify(result.data),
                id: resultLogApi?.id
            })
            return result.data;
        } catch (e) {
            await manager.apiLog.saveLogError({
                apiRoute: url,
                system: EApiLogSystem.WEBCONTROL,
                request: JSON.stringify(body),
                response: e?.response?.data ? JSON.stringify(e.response.data) : e.message,
                remark: 'Error couponLists',
            })
            throw e;
        }
    }
}
