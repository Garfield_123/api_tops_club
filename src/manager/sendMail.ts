import { LessThan } from "typeorm";
import { SendMail } from "../entity/SendMail";

export enum EMailType {
  NEW_MEMBER = "new_member",
  ORDER_RECEIVED = "order_received",
  ORDER_CANCELLED = "order_cancelled",
  ORDER_CANCELLED_PAYMENT = "order_cancelled_payment",
}

export class SendMailManager {
  async save(
    mailType: EMailType,
    ref: string,
    customerId: number
  ): Promise<any> {
    try {
      const repos = global.repos;
      const newMail = new SendMail();
      newMail.mail_type = mailType;
      newMail.customer_id = customerId;
      newMail.ref = ref;
      newMail.is_sent = false;
      newMail.send_count = 0;
      newMail.send_at = new Date();
      newMail.created_at = new Date();
      newMail.updated_at = new Date();
      await repos.sendMail.save(newMail);
    } catch (e) {
      throw e;
    }
  }
}
