import { ESegment } from "../types/customer";
import { manager } from ".";
import { ESectionType, ISectionHomePage } from "../types/section";
import { Section } from "../entity/Section";

export class HomeManager {
    async getPageDetail(screenId: number, customerId: number, lang: string): Promise<any> {
      try {
        const repos = global.repos;
        const sections: ISectionHomePage[] = await repos.section.createQueryBuilder('section')
            .select([
                'id as section_id', 
                'title_en as section',
                'title_th',
                'title_en',
                'type as section_type',
                'sequence'
            ])
            .where('screen_id = :screenId and is_active = true', {
                screenId,
            })
            .orderBy('sequence', 'ASC')
            .getRawMany();
        
        const customer = customerId ? await manager.customer.findByCustomerId(customerId) : null;
        const customerSegment = await manager.customer.findSegment(customer?.segment_code || ESegment.GUESS);
        await Promise.all(sections.map(async section => {
            switch (section.section_type) {
                  case ESectionType.BRAND:
                        section = await manager.section.getContentBrand(lang, section, customerSegment);
                        break;
                  case ESectionType.CATEGORY:
                        section = await manager.section.getContentCategory(section, customerSegment);
                        break;
                  case ESectionType.COUNTRY:
                        section = await manager.section.getContentCountry(lang, section, customerSegment);
                        break;
                  case ESectionType.IMAGE:
                        section = await manager.section.getContentImage(section, customer, customerSegment);
                        break;
                  case ESectionType.PRODUCT:
                        section = await manager.section.getContentProduct(section, customer, customerSegment);
                        break;
                  default:
                        section = await manager.section.getContentDefault(section, customer, customerSegment);
            }
        }))
        return sections;
      } catch (e) {
        throw e;
      }
    }
  }
  