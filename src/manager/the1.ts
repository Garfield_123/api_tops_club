import axios from "axios";
import { config } from "../config";
import { token } from "../utils/token";
import { TheOne } from "../entity/TheOne";
import { EState } from "../types/the1";
import { manager } from "./";
import { addDays, addHours, addYears, differenceInCalendarDays, format } from 'date-fns'
import { EApiLogSystem } from "../types/apiLog";
import * as https from "https";
import { ESegment } from "../types/customer";

const AWS = require("aws-sdk");

export class The1Manager {
    async getAccessTokenByOAuth(req, res) {
        try {
            const { code, state } = req.query;
            if (!EState[state?.toUpperCase()]) {
                const response = {
                    code: 400,
                    data: {},
                    message: "State is invalid.",
                }
                await manager.apiLog.saveLog({
                    apiRoute: req.originalUrl,
                    remark: 'The1 Callback',
                    request: JSON.stringify(req.query),
                    system: EApiLogSystem.T1C,
                    response: JSON.stringify(response)
                })
                return res.status(400).json(response);
            }
            
            const result = await this.getAccessToken(code);
            const generated = await this.generateToken(state, result)
            if (generated.isValid) {
                if (generated?.redirectUrl) {
                    await manager.apiLog.saveLog({
                        apiRoute: req.originalUrl,
                        remark: 'The1 Callback',
                        request: JSON.stringify(req.query),
                        system: EApiLogSystem.T1C,
                        response: generated.redirectUrl
                    });
                    return res.redirect(generated.redirectUrl)
                }

                const response = {
                    code: 200,
                    data: generated.data,
                    message: generated.message,
                }
                await manager.apiLog.saveLog({
                    apiRoute: req.originalUrl,
                    remark: 'The1 Callback',
                    request: JSON.stringify(req.query),
                    system: EApiLogSystem.T1C,
                    response: JSON.stringify(response)
                })
                return res.status(200).json(response)
            } else {
                const response = {
                    code: 400,
                    data: generated.data,
                    message: generated.message,
                }
                await manager.apiLog.saveLog({
                    apiRoute: req.originalUrl,
                    remark: 'The1 Callback',
                    request: JSON.stringify(req.query),
                    system: EApiLogSystem.T1C,
                    response: JSON.stringify(response)
                })
                return res.status(400).json(response)
            }
        } catch (e) {
            throw e;
        }
    }

    async getAccessToken(code) {
        try {
            const response = await axios.post(config.the1.tokenUrl, {
                grant_type: 'authorization_code',
                redirect_uri: `${config.selfUrl}/the1/auth`,
                code: code
            }, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': `Basic ${Buffer.from(`${config.the1.clientId}:${config.the1.clientSecret}`).toString('base64')}`
                }
            })
            return response.data;
        } catch (e) {
            throw e;
        }
    }

    async requestOtp(req, res) {
        try {
            const { country, value } = req.body;
            if (!country || !value) {
                return res.status(400).json({
                    code: 400,
                    data: {},
                    message: "Request body is invalid.",
                });
            }

            const userInfo: any = await this.getUserAuthenInfo({
                username: {
                    country: country,
                    value: value
                }
            });

            if (!userInfo) {
                return res.status(400).json({
                    code: 400,
                    data: {},
                    message: "Cannot get the user info.",
                });
            }

            if (userInfo?.error) {
                return res.status(400).json({
                    code: 400,
                    data: userInfo.error,
                    message: userInfo.error?.description,
                });
            }

            const { authenticate, username, status, type } = userInfo;
            const otp = authenticate.otp;
            if (!(otp && otp === true && status && status === "active" && type && type === "member")) {
                return res.status(400).json({
                    code: 400,
                    data: {},
                    message: "Request OTP is not success.",
                });
            }

            try {
                // const findCustomer = await manager.customer.checkMember(`${username?.country}${username?.value}`);
                // if (findCustomer && findCustomer.t1_access_token)  {
                //     return res.status(400).json({
                //         code: 400,
                //         data: {},
                //         message: 'This account is already logged in.',
                //     });
                // }

                const result: any = await this.getRequestOtp({
                    on: {
                        country: country,
                        value: value
                    },
                    type: "mobile"
                });

                if (result?.error) {
                    return res.status(400).json({
                        code: 400,
                        data: result.error,
                        message: result.error?.description || 'Request OTP error',
                    });
                } else {
                    const { requestID, otpRefText } = result
                    return res.status(200).json({
                        code: 200,
                        data: {
                            request_id: requestID,
                            otp_ref_text: otpRefText
                        },
                        message: "Request OTP is success.",
                    });
                }
            } catch (error) {
                return res.status(400).json({
                    code: 400,
                    data: {},
                    message: "Request OTP is invalid.",
                });
            }
        } catch (e) {
            return res.status(500).json({
                code: 500,
                data: {},
                message: "Request body is invalid.",
            });
        }
    }

    async loginOtp(req: any, res: any) {
        try {
            const { otp_request_id, otp_code, device_id }: {
                otp_request_id: string;
                otp_code: string;
                device_id: string;
            } = req.body;

            if (!otp_request_id || !otp_code) {
                return res.status(400).json({
                    code: 400,
                    data: {},
                    message: "Request body is invalid.",
                });
            }

            const requestParams: any = new URLSearchParams({
                grant_type: 'otp',
                scope: 'openid',
                otp_request_id: otp_request_id,
                otp_code: otp_code,
            });

            const result: any = await this.getToken(requestParams);
            const generated = await this.generateToken(EState.SIGNIN, result, device_id);
            if (generated.isValid) {
                if (generated?.redirectUrl) {
                    await manager.customer.saveDeviceId(generated.customerId, device_id);
                    return res.redirect(generated.redirectUrl)
                }

                if (device_id && generated.lastDeviceId && generated.lastDeviceId !== device_id) {
                    global.io.emit('auto-signout', {
                        customerId: generated.customerId,
                        deviceId: generated.lastDeviceId,
                    });
                }
                await manager.customer.saveDeviceId(generated.customerId, device_id);
                
                const response = {
                    code: 200,
                    data: generated.data,
                    tmpCartRef: generated.tmpCartRef,
                    message: generated.message,
                }
                return res.status(200).json(response)
            } else {
                const response = {
                    code: 400,
                    data: generated.data,
                    message: generated.message,
                }
                return res.status(400).json(response)
            }
        } catch (e) {
            return res.status(500).json({
                code: 500,
                data: {},
                message: `Error: ${e.message}`, 
            });
        }
    }

    async refreshToken (req: any, res: any) {
        try {
            const { refCode } = req.payloadDecoded;
            const requestParams: any = new URLSearchParams({
                grant_type: 'refresh_token',
                scope: 'openid',
                refresh_token: refCode,
            });

            const result: any = await this.getToken(requestParams);
            const generated = await this.generateToken(EState.SIGNIN, result)
            if (generated.isValid) {
                if (generated?.redirectUrl) {
                    return res.redirect(generated.redirectUrl)
                }

                const response = {
                    code: 200,
                    data: generated.data,
                    tmpCartRef: generated.tmpCartRef,
                    message: generated.message,
                }
                return res.status(200).json(response)
            } else {
                const response = {
                    code: 400,
                    data: generated.data,
                    message: generated.message,
                }
                return res.status(400).json(response)
            }
        } catch (e) {
            return res.status(500).json({
                code: 500,
                data: {},
                message: `Error: ${e.message}`, 
            });
        }
    }

    async getUserAuthenInfo (body) {
        try {
            const response: any = await this.getRequestAWS('POST', "auth/api/auth/services/autheninfo", "application/json", `Basic ${config.the1.passport.xauthorizationkey}`, JSON.stringify(body));
            return response?.data || response;
        } catch (e) {
            throw e;
        }
    }

    async getRequestOtp(body) {
        try {
            const response: any = await this.getRequestAWS('POST', "auth/api/auth/services/requestotp", "application/json", `Basic ${config.the1.passport.xauthorizationkey}`, JSON.stringify(body));
            return response?.data || response;
        } catch (e) {
            throw e;
        }
    }

    async getToken(body) {
        try {
            const response: any = await this.getRequestAWS('POST', "auth/token", "application/x-www-form-urlencoded", `Basic ${config.the1.passport.xauthorizationkey}`, body.toString());
            return response?.data || response;
        } catch (e) {
            throw e;
        }
    }

    async getRequestAWS(method: string, path: string, content_type: string, authKey: string, data?: string) {
        return new Promise(async (resolve, reject) => {
            try {
                const endpoint = new AWS.Endpoint(config.the1.passport.domain);
                let request = new AWS.HttpRequest(endpoint, config.the1.passport.region);
                request.method = method;
                request.path += `${config.the1.passport.path}/${path}`;

                if (data) {
                    request.body = data;
                    request.headers["Content-Length"] = Buffer.byteLength(request.body);
                }

                request.headers["host"] = config.the1.passport.domain;
                request.headers["Content-Type"] = content_type;
                 request.headers["x-authorization-key"] = authKey;
                request.headers["x-api-key"] = config.the1.passport.xapikey;
                const credentials = new AWS.Credentials(
                    config.the1.passport.accessKeyId,
                    config.the1.passport.secretKeyId
                );
                let signer = new AWS.Signers.V4(request, config.the1.passport.serviceName);
                signer.addAuthorization(credentials, new Date());
                let client = new AWS.HttpClient();
                client.handleRequest(
                    request,
                    null,
                    (response) => {
                        let responseBody = "";
                        response.on("data", (chunk) => {
                            responseBody += chunk;
                        });
                        response.on("end", async () => {
                            try {
                                const res = JSON.parse(responseBody);
                                resolve(res);
                            } catch (ex) { }

                        });
                    },
                    () => {
                        resolve(null);
                    }
                );
            } catch (e) {
                reject(e);
            }
        });
    }

    async getProfile(body: string) {
        try {
            const url = `https://${config.the1.passport.domain}/${config.the1.passport.path}/member/partner/accounts/user/profile`
            const headers = {
                "x-api-key": config.the1.passport.xapikey,
                "x-authorization-key": `Bearer ${body}`,
                "trace-log-id": config.the1.clientId
            }
            const response = await axios.get(url, {
                headers,
            });

            return response.data;
        } catch (e) {
            throw e;
        }
    }

    async checkExistThe1 (userAccountID): Promise<any> {
        try {
            const repos = global.repos;
            const data = await repos.theOne.findOne({
                where: {
                    t1_user_account_id: userAccountID
                }
            })
            return data
        } catch (e) {
            throw e;
        }
    }

    async updateThe1Data (userInfo: any, data: any, refreshToken: string): Promise<any> {
        try {
            const repos = global.repos;
            let t1_style = "";
            let t1_segments = "";
            let t1_card = "";
            
            const {
                style,
                segments,
                cards,
                dateOfBirth,
                employeeBUShortCode,
                employeeID,
                gender,
                isStaff,
                memberLanguagePref,
                title,
                imageProfile,
                firstName,
                lastName,
                status,
                consentDate,
                userAccountID,
                onlineEmail,
                onlineMobile,
                consentFlag,
                consentVersion,
                dcsConsentVersion,
                consentChallenge
            } = data

            try {
                t1_style = JSON.stringify(style);
            } catch {
                t1_style = "";
            }
            try {
                t1_segments = JSON.stringify(segments);
            } catch {
                t1_segments = "";
            }
            try {
                t1_card = JSON.stringify(cards);
            } catch {
                t1_card = "";
            }

            const newData = new TheOne();
            newData.t1_card_no = cards && cards.length > 0 ? cards[0].cardNo : null;
            newData.t1_points_balance = cards && cards.length > 0 ? cards[0].pointsBalance : null;
            newData.t1_points_expiry_this_year = cards && cards.length > 0 ? cards[0].pointsExpiryThisYear : null;
            newData.t1_date_of_birth = dateOfBirth || null;
            newData.t1_employee_bu_short_code = employeeBUShortCode || null;
            newData.t1_employee_id = employeeID || null;
            newData.t1_gender = gender || null;
            newData.t1_is_staff = isStaff || null;
            newData.t1_member_language_pref = memberLanguagePref || null;
            newData.t1_style = t1_style
            newData.t1_title_en = title ? title.en : null;
            newData.t1_title_th = title ? title.th : null;
            newData.t1_segments = t1_segments;
            newData.t1_image_profile = imageProfile || null;
            newData.t1_first_name_th = firstName ? firstName.th : null;
            newData.t1_first_name_en = firstName ? firstName.en : null;
            newData.t1_last_name_th = lastName ? lastName.th : null;
            newData.t1_last_name_en = lastName ? lastName.en : null;
            newData.t1_status = status || null;
            newData.t1_card = t1_card;
            newData.t1_consent_date = consentDate || null;
            newData.t1_user_account_id = userAccountID || null;
            newData.t1_email = onlineEmail ? onlineEmail.value : null;
            newData.t1_email_verified = onlineEmail ? onlineEmail.verified : null;
            newData.t1_mobile_country = onlineMobile ? onlineMobile.country : null;
            newData.t1_mobile_value = onlineMobile ? onlineMobile.value : null;
            newData.t1_mobile_verified = onlineMobile ? onlineMobile.verified : null;
            newData.t1_consent_flag = consentFlag || null;
            newData.t1_consent_version = consentVersion || null;
            newData.t1_dcs_consent_version = dcsConsentVersion || null;
            newData.t1_consent_challenge = consentChallenge || null;
            newData.t1_refresh_token = refreshToken || null;

            if (userInfo) {
                newData.id = userInfo.id;
                newData.updated_by = "the1";
                newData.updated_at = new Date();
            } else {
                newData.created_by = "the1";
                newData.created_at = new Date();
                newData.updated_by = "the1";
                newData.updated_at = new Date();
            }

            const result = await repos.theOne.save(newData);
            return result;
        } catch (e) {
            throw e;
        }
    }

    async generateToken(state: string, data: any, deviceId?: string) {
        try {
            const { access_token, refresh_token, error, code, message } = data;
            if (!(access_token && refresh_token)) {
                return {
                    isValid: false,
                    data: {
                        code: error?.code || code,
                        description: error?.description || message,
                        name: error?.name || message,
                        info: {
                            "910101": "ไม่พบ otp_request_id หรือ หมดอายุ หรือ ใช้ไปแล้ว",
                            "010002": "รหัสผ่านไม่ถูกต้อง",
                            "010007": "รหัสผ่านไม่ถูกต้อง",
                            "010006": "รหัสผ่านไม่ถูกต้อง",
                            "010004": "รหัสผ่านไม่ถูกต้อง",
                            "020001": "ไม่พบบัญชีของคุณผูกกับThe1",
                            "010012": "เลขบัตรประชาชน หรือ เลขพาสปอร์ต ไม่ถูกต้อง",
                            "010003": "คุณใส่รหัสผิดเกิน 3 ครั้ง บัญชีถูกระงับการใช้งานชั่วคราว กรุณาลองใหม่อีกครั้งในอีก 15 นาที"
                        }
                    },
                    message
                }
            }

            try {
                const profile: any = await this.getProfile(access_token);
                if (!(profile && profile?.success)) {
                    return {
                        isValid: false,
                        data: {},
                        message: "Get profile failed.",
                    }
                }

                const { userAccountID } = profile.data;
                let userInfo: any = await this.checkExistThe1(userAccountID);
                userInfo = await this.updateThe1Data(userInfo, profile.data, refresh_token);
                userInfo = await manager.customer.saveCustomerDetail(profile.data, state === EState.SIGNIN ? access_token : null, state === EState.SIGNIN ? refresh_token : null);
                if (userInfo?.is_active === false) {
                    return {
                        isValid: false,
                        data: {},
                        message: "This user is not activated. Please contact administrator.",
                    }
                }
                
                if (state?.toLowerCase() === EState.SIGNIN) {
                    let segmentCode = userInfo.segment_code;
                    let memberExpiredDate = userInfo.membership_expired_date;
                    let isTrial = userInfo.is_trial;

                    if (userInfo.is_member_renewing) {
                        memberExpiredDate = addYears(new Date(memberExpiredDate), 1)
                        isTrial = false;
                        segmentCode = ESegment.MEMBER;
                    } 

                    const segment = await manager.customer.findSegment(segmentCode)
                    const payload = {
                        customer_id: userInfo.customer_id,
                        segment: segmentCode,
                        segment_name: segment?.segment_name,
                        first_name: userInfo.first_name,
                        last_name: userInfo.last_name,
                        first_name_en: userInfo.first_name_en,
                        last_name_en: userInfo.last_name_en,
                        email: userInfo.email,
                        mobile: userInfo.mobile,
                        t1_card_no: userInfo.t1_card_no,
                        t1_points_balance: userInfo.t1_points_balance,
                        is_trial: isTrial,
                        is_member_expired: differenceInCalendarDays(memberExpiredDate, addHours(new Date(), 7)) + 1 <= 0,
                        member_expired_date: memberExpiredDate,
                        member_remainning_days: differenceInCalendarDays(memberExpiredDate, addHours(new Date(), 7)) + 1,
                        renew_member_date: addDays(memberExpiredDate, 1),
                        ref_code: userInfo.t1_refresh_token,
                    }

                    await manager.customer.updateLogonLog(userInfo.customer_id, true)
                    const jwt = await token.gen({...payload});

                    const repos = global.repos;
                    const tmpOrder: any = await repos.order.findOne({
                        where: {
                            cus_id: userInfo.customer_id
                        },
                        order: {
                            order_date: 'DESC'
                        }
                    })
                    return {
                        isValid: true,
                        data: jwt,
                        tmpCartRef: tmpOrder && tmpOrder.status.trim() === '' ? tmpOrder.ref : null,
                        customerId: userInfo.customer_id,
                        lastDeviceId: userInfo?.last_device_id || null,
                        message: "Success",
                    }
                } else {
                    return {
                        isValid: true,
                        customerId: userInfo.customer_id,
                        lastDeviceId: deviceId,
                        redirectUrl: `${config.zbackUrl}/the1/register?status=success`,
                    }
                }
            } catch (error) {
                return {
                    isValid: false,
                    data: {},
                    message: error.message,
                }
            }
        } catch (e) {
            throw e;
        }
    }

    async updateCentralThe1Segment (cardNo: string, segmentCode: string): Promise<any> {
        try {
            const partnerCode = 'CFR';
            const requestTime = format(addHours(new Date(), 7), 'ddMMyyyy_HH:mm:ss:SSS')

            const body = {
                cardNo: cardNo,
                memberNo: '',
                segmentGroupName: segmentCode,
                segmentLevelID: segmentCode,
            }
            const headers = {
                sourceTransID: `${partnerCode}-${requestTime}`,
                requestTime: requestTime,
                partnerCode: partnerCode,
                languagePreference: 'TH',
                transactionChannel: '002',
                client_id: config.central.clientId,
                client_secret: config.central.clientSecret,
            }
            
            const url = `${config.central.url}/t1c/member/segment/1_0`
            const configApi = {
                headers: headers,
                data: body,
                httpsAgent: new https.Agent({
                    rejectUnauthorized: false,
                }),
            }
            try {
                const response = await axios.put(url, configApi.data, {
                    headers: configApi.headers,
                    httpsAgent: configApi.httpsAgent
                })
                await manager.apiLog.saveLog({
                    apiRoute: url,
                    remark: 'Update Central The1 Segment',
                    request: JSON.stringify({
                        headers: configApi.headers,
                        data: configApi.data,
                    }),
                    system: EApiLogSystem.T1C,
                    response: JSON.stringify(response.data)
                })
                return response.data;
            } catch (e) {
                await manager.apiLog.saveLog({
                    apiRoute: url,
                    remark: 'Update Central The1 Segment',
                    request: JSON.stringify({
                        headers: configApi.headers,
                        data: configApi.data,
                    }),
                    system: EApiLogSystem.T1C,
                    response: JSON.stringify(e?.response?.data)
                })
                return e?.response?.data;
            }
        } catch (e) {
            throw e;
        }
    }

    async deleteCentralThe1Segment (cardNo: string, segmentCode: string): Promise<any> {
        try {
            const partnerCode = 'CFR';
            const requestTime = format(addHours(new Date(), 7), 'ddMMyyyy_HH:mm:ss:SSS')

            const body = {
                cardNo: cardNo,
                memberNo: '',
                segmentGroupName: segmentCode,
                segmentLevelID: segmentCode,
            }
            const headers = {
                sourceTransID: `${partnerCode}-${requestTime}`,
                requestTime: requestTime,
                partnerCode: partnerCode,
                languagePreference: 'TH',
                transactionChannel: '002',
                client_id: config.central.clientId,
                client_secret: config.central.clientSecret,
            }
            
            const url = `${config.central.url}/t1c/member/segment/1_0`
            const configApi = {
                headers: headers,
                data: body,
                httpsAgent: new https.Agent({
                  rejectUnauthorized: false,
                }),
            }
            try {
                const response = await axios.delete(url, configApi)
                await manager.apiLog.saveLog({
                    apiRoute: url,
                    remark: 'Delete Central The1 Segment',
                    request: JSON.stringify({
                        headers: configApi.headers,
                        data: configApi.data,
                    }),
                    system: EApiLogSystem.T1C,
                    response: JSON.stringify(response.data)
                })
                return response.data;
            } catch (e) {
                await manager.apiLog.saveLog({
                    apiRoute: url,
                    remark: 'Delete Central The1 Segment',
                    request: JSON.stringify({
                        headers: configApi.headers,
                        data: configApi.data,
                    }),
                    system: EApiLogSystem.T1C,
                    response: JSON.stringify(e?.response?.data)
                })
                return e?.response?.data;
            }
        } catch (e) {
            throw e;
        }
    }

    async signOut(customerId) {
        try {
            const result = {
                passed: true,
                errorMessage: null,
            }
            const customer = await manager.customer.findByCustomerId(customerId);

            if (!customer) {
                result.passed = false;
                result.errorMessage = 'Customer does not exist.'
                return result
            }

            if (!customer.t1_access_token) {
                result.passed = false;
                result.errorMessage = 'Access token does not exist.'
                return result
            }

            const response: any = await this.getRequestAWS('GET', "auth/api/sessions/user/signout", "application/json", `Bearer ${customer.t1_access_token}`);
            if (!response?.success) {
                result.passed = false;
                result.errorMessage = `${response?.error?.code} - ${response?.error?.description}`;
                return result
            }
            return result;
        } catch (e) {
            throw e;
        }
    }
}
