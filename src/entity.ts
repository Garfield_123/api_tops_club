import { DataSource } from "typeorm";
import { TheOne } from "./entity/TheOne";
import { Brands } from "./entity/Brands";
import { Categories } from "./entity/Categories";
import { Countries } from "./entity/Countries";
import { MappingProducts } from "./entity/MappingProducts";
import { Customer } from "./entity/Customer";
import { CustomerSegment } from "./entity/CustomerSegment";
import { ProductDiscount } from "./entity/ProductDiscount";
import { ProductFavorite } from "./entity/ProductFavorite";
import { SendMail } from "./entity/SendMail";
import { ApiLogs } from "./entity/ApiLogs";
import { BadgeBuyDisc } from "./entity/BadgeBuyDisc";
import { BadgeBuyFree } from "./entity/BadgeBuyFree";
import { Order } from "./entity/Order";
import { CustomerLogonLogs } from "./entity/CustomerLogonLogs";
import { Section } from "./entity/Section";
import { MemberTrial } from "./entity/MemberTrial";
import { MemberBenefit } from "./entity/MemberBenefit";
import { TermsCondition } from "./entity/TermsCondition";
import { CustomerDeleteAccountLogs } from "./entity/CustomerDeleteAccountLogs";
import { CustomerDeleteReasons } from "./entity/CustomerDeleteReasons";
import { CustomerDeleted } from "./entity/CustomerDeleted";

export const entities = [
  TheOne,
  Brands,
  Categories,
  Countries,
  MappingProducts,
  Customer,
  CustomerSegment,
  ProductDiscount,
  ProductFavorite,
  SendMail,
  ApiLogs,
  BadgeBuyDisc,
  BadgeBuyFree,
  Order,
  CustomerLogonLogs,
  Section,
  MemberTrial,
  MemberBenefit,
  TermsCondition,
  CustomerDeleteAccountLogs,
  CustomerDeleteReasons,
  CustomerDeleted,
];

export function setupRepositories(orm: DataSource) {
  return {
    theOne: orm.getRepository(TheOne),
    brands: orm.getRepository(Brands),
    categories: orm.getRepository(Categories),
    countries: orm.getRepository(Countries),
    mappingProducts: orm.getRepository(MappingProducts),
    customer: orm.getRepository(Customer),
    customerSegment: orm.getRepository(CustomerSegment),
    productDiscount: orm.getRepository(ProductDiscount),
    productFavorite: orm.getRepository(ProductFavorite),
    sendMail: orm.getRepository(SendMail),
    apiLog: orm.getRepository(ApiLogs),
    badgeBuyDisc: orm.getRepository(BadgeBuyDisc),
    badgeBuyFree: orm.getRepository(BadgeBuyFree),
    order: orm.getRepository(Order),
    customerLogonLog: orm.getRepository(CustomerLogonLogs),
    section: orm.getRepository(Section),
    memberTrial: orm.getRepository(MemberTrial),
    memberBenefit: orm.getRepository(MemberBenefit),
    termsCondition: orm.getRepository(TermsCondition),
    customerDeleteAccountLog: orm.getRepository(CustomerDeleteAccountLogs),
    customerDeleteReason: orm.getRepository(CustomerDeleteReasons),
    customerDeleted: orm.getRepository(CustomerDeleted),
  };
}
