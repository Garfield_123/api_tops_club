import * as dotenv from "dotenv";
dotenv.config();

export const config = {
  environment: process.env.ENVIRONMENT,
  selfUrl: process.env.SELF_URL,
  zbackUrl: process.env.ZBACK_URL,
  cmsUrl: process.env.CMS_URL,
  backendUrl: process.env.BACKEND_URL,
  port: parseInt(process.env.PORT || "3000"),
  db: {
    host: process.env.DB_HOST,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    port: parseInt(process.env.DB_PORT || "5432"),
  },
  jwt: {
    secret: process.env.JWT_SECRET,
    audience: process.env.JWT_AUDIENCE,
    algorithm: process.env.JWT_ALGORITHM,
  },
  the1: {
    clientId: process.env.THE1_CLIENT_ID,
    clientSecret: process.env.THE1_CLIENT_SECRET,
    tokenUrl: process.env.THE1_TOKEN_URL,
    passport: {
      path: process.env.THE1_PASSPORT_PATH,
      region: process.env.THE1_PASSPORT_REGION,
      domain: process.env.THE1_PASSPORT_DOMAIN,
      xapikey: process.env.THE1_PASSPORT_API_KEY,
      accessKeyId: process.env.THE1_PASSPORT_ACCESS_KEY,
      secretKeyId: process.env.THE1_PASSPORT_SECRET_KEY,
      xauthorizationkey: process.env.THE1_PASSPORT_AUTH_KEY,
      // product: process.env.THE1_PASSPORT_PRODUCT,
      // partnerCode: process.env.THE1_PASSPORT_PARTNER_CODE,
      serviceName: process.env.THE1_PASSPORT_SERVICE_NAME,
    },
  },
  central: {
    url: process.env.CENTRAL_URL,
    clientId: process.env.CENTRAL_CLIENT_ID,
    clientSecret: process.env.CENTRAL_CLIENT_SECRET,
  },
  obs: {
    accessKeyId: process.env.OBS_ACCESS_KEY,
    secretAccessKey: process.env.OBS_SECRET_KEY,
    server: process.env.OBS_SERVER_NAME,
    bucket: process.env.OBS_BUCKET_NAME,
  },
  payment2C2P: {
    merchantId: process.env["2C2P_MERCHANT_ID"],
    secretKey: process.env["2C2P_SECRET_KEY"],
  },
  emailCron: {
    tokenKey: process.env.EMAIL_TOKEN_KEY,
  },
  algoliaKey: {
    ALGOLIA_CLIENT_ID: process.env.ALGOLIA_CLIENT_ID,
    ALGOLIA_CLIENT_SECRET: process.env.ALGOLIA_CLIENT_SECRET,
    ALGOLIA_INDEX: process.env.ALGOLIA_INDEX,
  },
  webControl: {
    token: process.env.WEBCONTROL_TOKEN
  },
  sts: {
    token: process.env.STS_TOKEN
  },
  crypto: {
    key: process.env.APP_CRPT_KEY,
    iv: process.env.APP_CRPT_IV,
  }
};
