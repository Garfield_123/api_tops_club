// import * as express from "express";
// const router = express.Router();
// // const { producerMain } = require("../../kafka");
// import { obsClient } from "../../utils/obs";
// import { manager } from "../../manager";
// import { config } from '../../config';

// const {
//     validateData,
//     checkHeader
// } = require("../../functions");
// router.post('/', [checkHeader], async function (req, res) {
//     const { t1_user_account_id } = req.body;
//     const file = req.file;
//     await validateData(
//         [t1_user_account_id, file],
//         req,
//         res
//     );
//     try {
//         const is_image = await obsClient.checkFileIsImage(file.mimetype);
//         if (is_image !== "png" && is_image !== "jpeg") {
//             const resp = {
//                 "message": "image format not match",
//                 "code": 404,
//                 "data": {}
//             }
//             res.status(404).json(resp);
//             return;
//         }
//         const image_stream = await obsClient.bufferToStream(file.buffer);
//         const put_object = await obsClient.putObject(`image_profile/${t1_user_account_id}.${is_image}`, image_stream);
//         if (put_object !== 200) {
//             const resp = {
//                 "message": "upload img to obs fail",
//                 "code": 404,
//                 "data": {
//                     status_obs: put_object
//                 }
//             }
//             res.status(404).json(resp);
//             return;
//         }
//         const path = `https://${config.obs.bucket}.${config.obs.server}image_profile/${t1_user_account_id}.${is_image}`;
//         await manager.users.updatePathProfile(t1_user_account_id, path);
//         const resp = {
//             "message": "upload success",
//             "code": 200,
//             "data": {
//                 "t1_user_account_id": t1_user_account_id,
//                 "file": `${t1_user_account_id}.${is_image}`
//             }
//         }
//         res.status(200).json(resp);
//         return;
//     } catch (error) {
//         const resp = {
//             "message": "KAFKA not connect",
//             "code": 404,
//             "data": {}
//         }
//         res.status(404).json(resp);
//         return;
//     }
// });
// export = router;
