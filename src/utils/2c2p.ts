import { config } from "../config";

class Payment2C2P {
    hash = async (payload?: any): Promise<any> => {
        try {
            const hashString = Object.keys(payload)
                .sort()
                .map(key => `${key}=${payload[key]}`)
                .join('&');
            const hmac = require('crypto')
                .createHmac('sha1', config.payment2C2P.secretKey)
                .update(hashString)
                .digest('hex');
            return hmac
        } catch (error) {
            throw error;
        }
    }
}

export const payment2C2P = new Payment2C2P();
