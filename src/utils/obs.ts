const { Duplex } = require('stream');
import { config } from '../config';
var ObsClient = require('esdk-obs-nodejs');
import { v4 as uuid } from "uuid";
import * as fs from "fs";

class Obs {
    obsPath: string;
    constructor() {
      this.obsPath = `https://${config.obs.bucket}.${config.obs.server}/`;
    }

    checkImageExtension = async (mimetype: string): Promise<any> => {
        let file_extension = "";
        switch (mimetype) {
            case "image/bmp":
                file_extension = "bmp";
                break;
            case "image/gif":
                file_extension = "gif";
                break;
            case "image/jpeg":
                file_extension = "jpeg";
                break;
            case "image/png":
                file_extension = "png";
                break;
            case "image/tiff":
                file_extension = "tiff";
                break;
            case "image/svg+xml":
                file_extension = "svg";
                break;
            default:
                file_extension = "csv";
                break;
        }
        return file_extension;
    }

    bufferToStream = async (buffer: string): Promise<any> => {
        const stream = new Duplex();
        stream.push(buffer);
        stream.push(null);
        return stream;
    }

    obsClient = async (): Promise<any> => {
        const obsClient = new ObsClient({
            access_key_id: config.obs.accessKeyId,
            secret_access_key: config.obs.secretAccessKey,
            server: config.obs.server,
        });
        return obsClient;
    }

    putObject = async (rootPath: string, file: any) => {
        let result = null;
        let filePath = null;
        try {
          const extention = await this.checkImageExtension(file?.mimetype);
          if (!(file && extention)) {
            return null;
          }
    
          const fileName = `${uuid()}_${Date.now()}.${extention}`;
          filePath = `./${fileName}`;
          fs.writeFileSync(filePath, file.data);
    
          const obs = await this.obsClient();
          const obsKey = `${rootPath}/${fileName}`;
          await obs.putObject({
            Bucket: config.obs.bucket,
            Key: obsKey,
            Body: fs.createReadStream(filePath),
          });
    
          result = `${this.obsPath}${obsKey}`;
        } catch (e) {
        } finally {
          if (filePath && fs.existsSync(filePath)) {
            fs.unlinkSync(filePath);
          }
    
          return result;
        }
      };

    getPublicPath = async (obsKey: string) => {
        try {
            const obs = await this.obsClient();
            const response = await obs.getObjectMetadata({
                Bucket: config.obs.bucket,
                Key: obsKey,
            });
            if (!(response.CommonMsg.Status === 200 && response.InterfaceResult)) {
                return null;
            }
        
            return `${this.obsPath}${obsKey}`;
        } catch (e) {
            return null;
        }
      };
    
    deleteObject = async (pathUrl: string) => {
        try {
            if (!pathUrl || !pathUrl.includes(this.obsPath)) {
                return;
            }

            const obsKey = pathUrl.replace(this.obsPath, "");
            const obs = await this.obsClient();
            await obs.deleteObject({
                Bucket: config.obs.bucket,
                Key: obsKey,
            });
        } catch (e) {}
    };

    listObjects = async () => {
        return new Promise(async (resolve, reject) => {
            try {
                const obs = await obsClient.obsClient();
                obs.listObjects({
                    Bucket: config.obs.bucket,
                    // MaxKeys : 100
                    Prefix: 'image_profile/'
                }, (err, result) => {
                    if (err) {
                        // console.error('Error-->' + err);
                    } else {
                        // console.log('Status-->' + result.CommonMsg.Status);
                        if (result.CommonMsg.Status < 300 && result.InterfaceResult) {
                            // const { Contents = [] } = result.InterfaceResult;
                            // for (let i = 0; i < Contents.length; i++) {
                                // console.log('Contents[' + i + ']:');
                                // console.log('Key-->' + Contents[i]['Key']);
                                // console.log('LastModified-->' + Contents[i]['LastModified']);
                                // console.log('Size-->' + result.InterfaceResult.Contents[i]['Size']);
                            // }
                        }
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    getPath = async (key: string) => {
        return new Promise(async (resolve, reject) => {
            try {
                const obs = await obsClient.obsClient();
                obs.getObjectMetadata({
                    Bucket: config.obs.bucket,
                    // prefix: Optional. It indicates the name of the folder before the object.
                    Key: key
                }, (err, result) => {
                    if (err) {
                        // console.error('Error-->' + err);
                        resolve(err)
                    } else {
                        // console.log('Status-->' + result.CommonMsg.Status);
                        if (result.CommonMsg.Status < 300 && result.InterfaceResult) {
                            // console.log(result.InterfaceResult.ContentType);
                            // console.log(result.InterfaceResult.ContentLength);
                            // console.log(result.InterfaceResult.Metadata['property']);
                            const path = `${this.obsPath}}/${key}`;
                            resolve(path)
                        } else {
                            resolve(result)
                        }
                    }
                });
                // console.log('Permanent public URL:', publicUrl);
                // resolve(publicUrl)
            } catch (e) {
                reject(e);
            }
        });
    }
}

export const obsClient = new Obs();