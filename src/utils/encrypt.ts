import * as bcrypt from 'bcryptjs';
const cryptoJS = require("crypto-js");
import { config } from "../config";

export enum EDecryptType {
  TEXT = 0,
  JSON = 1
}

export class Encrypt {
  saltRounds: number = 10;
  genEncryptText(text): string {
    return bcrypt.hashSync(text, this.saltRounds);
  }

  compareEncryptText(text, hash): string {
    return bcrypt.compareSync(text, hash);
  }

  decryptCBC(decryptType: EDecryptType, encryptedText) {
    const keyParse = cryptoJS.enc.Base64.parse(config.crypto.key);
    const ivParse = cryptoJS.enc.Base64.parse(config.crypto.iv);
    const base64 = cryptoJS.enc.Base64.parse(encryptedText);
    const src = cryptoJS.enc.Base64.stringify(base64);
    const decrypt = cryptoJS.AES.decrypt(src, keyParse, {iv:ivParse, mode: cryptoJS.mode.CBC});
    const planText = decrypt.toString(cryptoJS.enc.Utf8)
    return decryptType === EDecryptType.JSON ? JSON.parse(planText) : planText;
  }
}

const encrypt = new Encrypt();
export default encrypt;
