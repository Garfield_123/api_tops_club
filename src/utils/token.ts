import * as jwt from 'jsonwebtoken';
import { IJwtOption } from '../types/token';
import { v4 as uuid } from 'uuid';
import { config } from "../config";
import { addHours } from 'date-fns';

class Token {
    gen = async (payload: any): Promise<string> => {
        const date = addHours(new Date(), 7);
        date.setHours(date.getHours() + 1);
    
        payload['exp'] = date.getTime();
        return jwt.sign(payload, config.jwt.secret, this.options() as any);
    }

    get = (token: string): any => {
        const result = jwt.decode(token.substring(token.indexOf(' ') + 1), config.jwt.secret);
        return result;
    }

    async verify(auth: string) {
        const verifyOptions = this.options();
        delete verifyOptions.jwtid;
    
        const verified = jwt.verify(auth, config.jwt.secret, verifyOptions);
        return verified;
    }

    isTokenExpire(auth: any): boolean {
        return auth && addHours(new Date(), 7).getTime() > auth.exp;
    }

    options = (jwtId = null): IJwtOption => {
        return {
            jwtid: jwtId || uuid(),
            audience: config.jwt.audience,
            algorithm: config.jwt.algorithm || 'HS256',
        }
    }
}

export const token = new Token();
