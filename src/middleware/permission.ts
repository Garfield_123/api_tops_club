export const checkPermission = (permissionCode: string) => {
    return async (req: any, res: any, next: any) => {
        try {
            const result = true

            if (!result) {
                return res.status(403).json({
                    message: 'คุณไม่มีสิทธิ์ในการใช้งาน กรุณาติดต่อผู้ดูแลระบบ'
                });
            } else {
                next();
            }
        } catch (e) {
            next(e);
        }
    };
}
