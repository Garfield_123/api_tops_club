import { Request, Response, NextFunction } from 'express';

export const validateFavicon = (req: Request, res: Response, next: NextFunction) => {
  if (req.originalUrl && req.originalUrl.split('/').pop() === 'favicon.ico') {
    res.sendStatus(204);
    return;
  }
  next();
};
