import { addHours } from "date-fns";
import { token } from "../utils/token";
import encrypt, { EDecryptType } from "../utils/encrypt";
import { manager } from "../manager";
const { unless } = require('express-unless');

export const authMiddleware = () => {
    const middleware = async (req, res, next) => {
        try {
            const authToken = req.headers.authorization;
            if (!authToken) {
                res.status(401).json({ message: 'Unauthorized.' });
                return;
            }
          
            try {
                const tokenValue = authToken.substring(authToken.indexOf(' ') + 1);
                const verified = await token.verify(tokenValue);
                if (token.isTokenExpire(verified)) {
                    res.status(401).json({ message: 'Token is expired.' });
                    return;
                }

                req.user = verified;
                return next();
            } catch (err) {
                res.status(401).json({ message: 'Token is invalid.' });
                return;
            }
        } catch (error) {
          return res.status(500).json({
            message: error.message,
          });
        }
      };
    
      middleware.unless = unless;
      return middleware;
};

export const validateAuthForRefreshToken = async (req, res, next) => {
    try {
        const authToken = req.headers.authorization;
        if (!authToken) {
            res.status(401).json({ message: 'Unauthorized.' });
            return;
        }

        try {
            const tokenValue = authToken.substring(authToken.indexOf(' ') + 1);
            const verified = await token.verify(tokenValue);

            if (!req.body) {
                res.status(401).json({ message: 'Unauthorized.' });
                return;
            }

            const { payload } = req.body;
            const decoded = await encrypt.decryptCBC(EDecryptType.JSON, payload);
            if (!decoded) {
                res.status(401).json({ message: 'Unauthorized.' });
                return;
            }

            if (!decoded?.timeStamp || !decoded?.refCode || !decoded?.customerId) {
                res.status(401).json({ message: 'Unauthorized.' });
                return;
            }

            if (decoded.refCode !== verified.ref_code) {
                res.status(401).json({ message: 'Unauthorized.' });
                return;
            }

            req.payloadDecoded = decoded;
            return next();
        } catch (err) {
            res.status(401).json({ message: 'Token is invalid.' });
            return;
        }
    } catch (error) {
        return res.status(500).json({
          message: error.message,
        });
    }
}

export const validateDeviceId = async (req, res, next) => {
    try {
        const { customerId, deviceId } = req.payloadDecoded;

        const customerInfo = await manager.customer.findByCustomerId(customerId);

        if (!customerInfo) {
            return res.status(400).json({ message: 'Customer is not exists.' });
        }

        if (deviceId && customerInfo?.last_device_id && deviceId === customerInfo?.last_device_id) {
            global.io.emit('auto-signout', {
                customerId: customerId,
                deviceId: customerInfo.last_device_id,
            });
            return res.status(400).json({ message: 'Device ID does not match.' });
        }
        return next()
    } catch (error) {
        return res.status(500).json({
          message: error.message,
        });
    }
}
