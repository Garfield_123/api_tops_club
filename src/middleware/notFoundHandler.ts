export const notFoundHandler = (req, res, next) => {
    return res.status(404).json({
        message: `Path: ${req.url} does not found.`
    });
}
