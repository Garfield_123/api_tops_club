export const methodNotAllowed = (req, res, next) => {
    return res.status(405).json({
        message: 'Method not allowed.'
    });
}
 