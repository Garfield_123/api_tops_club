export const errorHandler = (err, req, res, next) => {
  const status = err.status || 500;
  return res.status(status).json({
    code: status,
    message: err.message,
    data: {}
  });
};
