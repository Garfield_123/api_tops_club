export enum EState {
    SIGNUP = 'signup',
    SIGNIN = 'signin'
}

export interface IAuthUser {
    customer_id: number
};
