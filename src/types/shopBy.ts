export enum EShopByTab {
    CATEGORY = 'category',
    CATEGORIES = 'categories',
    COUNTRY = 'country',
    COUNTRIES = 'countries',
    BRAND = 'brand',
    BRANDS = 'brands',
}

export interface IRequestPLPDetail {
    type: EShopByTab,
    code: string,
    segment: string,
    customerId: number,
    lang: string,
    sortBy: ESortByType,
    orderBy: EOrderBy,
    priceMin: number,
    priceMax: number,
    ratingStart: number,
    brandCodes: string[],
    categoryCodes: string[],
    countryCodes: string[],
    isSimilar: boolean,
}

export interface IConditionShopByPLP {
    lang: string,
    sortBy: ESortByType,
    orderBy: EOrderBy,
    priceMin: number,
    priceMax: number,
    ratingStart: number,
    brandCodes: string[],
    categoryCodes: string[],
    countryCodes: string[],
    isSimilar?: boolean,
}

export enum ESortByType {
    RELEVANCE = 1,
    NEW_ARRIVALS,
    RATINGS,
    PRICE,
    NAME,
}

export enum EOrderBy {
    ASC = 'ASC',
    DESC = 'DESC',
}

export enum ELanguage {
    TH = 'TH',
    EN = 'EN',
}

export enum BadgeBuyDiscType {
    PERCENT = 'C',
    BATH = 'B',
}

export interface CountryFilterList {
    country_code: string;
    country_name_th: string;
    country_name_en: string;
    country_image_th?: string;
    country_image_en?: string;
    country_logo?: string;
    sequence: number;
    total_products: number;
}

export interface BrandFilterList {
    popular: BrandInfoFilterList[],
    nonPopular: BrandInfoFilterList[],
}

export interface BrandInfoFilterList {
    brand_code: string;
    brand_name_th: string;
    brand_name_en: string;
    brand_image_th?: string;
    brand_image_en?: string;
    is_popular: boolean;
    sequence: number;
    total_products: number;
};

export interface CategoryFilterList extends CategoryInfoFilterList {
    sub_category: CategoryInfoFilterList[];
}

export interface CategoryInfoFilterList {
    category_code: string;
    category_code_user?: string;
    category_name_th: string;
    category_name_en: string;
    category_image_th?: string;
    category_image_en?: string;
    category_icon?: string;
    sequence: number;
    total_products: number;
}
