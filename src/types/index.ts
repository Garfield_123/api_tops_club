export enum ELanguage {
    TH = 'TH',
    EN = 'EN',
}

export enum BadgeBuyDiscType {
    PERCENT = 'C',
    BATH = 'B',
}