export enum EApiLogSystem {
    MOBILE_APP = 'mobile-app',
    ZBACK_API = 'zback-api',
    APP_API = 'app-api',
    WEBCONTROL = 'webcontrol',
    STS = 'sts',
    PAYMENT_2C2P = '2C2P',
    T1C = 't1c'
}

export enum EApiLogType {
    INFO = 'info',
    ERROR = 'error',
}

export interface IReqSaveLogRequest {
    ref?: string;
    apiRoute: string;
    system: EApiLogSystem;
    type?: EApiLogType;
    request: string;
    remark: string;
}

export interface IReqSaveLogResponse {
    id: number;
    response: string;
}

export interface IReqSaveLogError {
    ref?: string;
    apiRoute: string;
    system: EApiLogSystem;
    request: string;
    response: string;
    remark: string;
}

export interface IReqSaveLog {
    ref?: string;
    apiRoute: string;
    type?: EApiLogType;
    system: EApiLogSystem;
    request: string;
    response: string;
    remark: string;
}
