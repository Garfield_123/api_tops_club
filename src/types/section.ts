export enum ESectionType {
    IMAGE = 'image',
    SKU = 'sku',
    CATEGORY = 'categories',
    BRAND = 'brand',
    COUNTRY = 'countries',
    MEMBER_FEE = 'member_fee',
    PRODUCT = 'products',
}

export interface ISectionHomePage {
    section_id: number;
    section: string;
    title_th: string;
    title_en: string;
    section_type: string;
    sequence: number;
    contents: any[]
}
