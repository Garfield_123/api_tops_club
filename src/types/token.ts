export interface IJwtOption {
    jwtid?: string;
    audience: string;
    algorithm: string;
}
