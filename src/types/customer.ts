export enum ESegment {
    GUESS = 'G01',
    TRIAL = 'Trial CLUB',
    MEMBER = 'Tops CLUB'
}

export enum ENotiType {
    APP = 'APP',
    EMAIL = 'EMAIL',
    SMS = 'SMS',
}