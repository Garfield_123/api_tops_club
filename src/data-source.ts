import "reflect-metadata"
import { DataSource } from "typeorm"
import { config } from "./config"

const rootFolder = config.environment === 'DEV' ? 'src' : 'dist';
export const AppDataSource = new DataSource({
    type: "postgres",
    host: config.db.host,
    port: config.db.port,
    username: config.db.username,
    password: config.db.password,
    database: config.db.database,
    synchronize: false,
    logging: ['error'],
    entities: [`${rootFolder}/entity/**/*{.ts,.js}`],
    migrations: [`${rootFolder}/migration/**/*{.ts,.js}`],
    subscribers: [],
})