import * as express from "express";
import { getHomePage } from "../controller/home";
import { methodNotAllowed } from "../middleware";

export const router = express.Router();
router.route("/").post(getHomePage).all(methodNotAllowed);
