import * as express from "express";
import { couponLists } from "../controller/backend";
import { methodNotAllowed } from "../middleware";

export const router = express.Router();
router.route("/coupon").post(couponLists).all(methodNotAllowed);
