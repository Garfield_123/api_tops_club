import * as express from "express";
import { getAccessTokenByOAuth, requestOtp, loginOtp, updateCentralThe1Segment, deleteCentralThe1Segment, signOut, refreshToken } from "../controller/the1";
import { methodNotAllowed, validateAuthForRefreshToken, validateDeviceId } from "../middleware";

export const router = express.Router();
router.route('/auth').get(getAccessTokenByOAuth).all(methodNotAllowed);
router.route('/request-otp').post(requestOtp).all(methodNotAllowed);
router.route('/login-otp').post(loginOtp).all(methodNotAllowed);
router.route('/refresh').post(validateAuthForRefreshToken, validateDeviceId, refreshToken).all(methodNotAllowed);
router.route('/signout').post(signOut).all(methodNotAllowed);
router.route('/central/segment').put(updateCentralThe1Segment).delete(deleteCentralThe1Segment).all(methodNotAllowed);
