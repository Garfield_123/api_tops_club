import * as express from "express";
import { getDetail, agoliaSearch } from "../controller/product";
import { methodNotAllowed } from "../middleware";

export const router = express.Router();
router.route("/detail/:mappingProductId").post(getDetail).all(methodNotAllowed);
router.route("/agoliasearch").post(agoliaSearch).all(methodNotAllowed);
