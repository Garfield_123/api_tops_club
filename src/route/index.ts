export { router as the1Router } from './the1';
export { router as shopByRouter } from './shopBy';
export { router as customerRouter } from './customer';
export { router as productRouter } from './product';
export { router as backendRouter } from './backend';
export { router as homeRouter } from './home';
// export { router as orderRouter } from './order';
