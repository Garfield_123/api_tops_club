import * as express from "express";
import { getShopBy, getShopByPLP, getFilterLists } from "../controller/shopBy";
import { methodNotAllowed } from "../middleware";

export const router = express.Router();
router.route('/plp').post(getShopByPLP).all(methodNotAllowed);
router.route('/filter').get(getFilterLists).all(methodNotAllowed);
router.route('/').get(getShopBy).all(methodNotAllowed);
