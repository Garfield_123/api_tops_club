import * as express from "express";
import {
  checkMember,
  saveProductFavorite,
  getProductFavorite,
  register,
  getOrderHistory,
  signOut,
  getMembershipInfo,
  saveNotification,
  removeCustomer,
  changeMemberEmail,
  getMultiAddress, 
  addMultiAddress,
  deleteMultiAdress,
  getBenefit,
  getTermsAndConditions,
  updateProfileImage,
} from "../controller/customer";
import { methodNotAllowed } from "../middleware";

export const router = express.Router();
// router.route('/order-history/detail').post(getOrderHistoryDetail).all(methodNotAllowed);
router.route("/order-history").post(getOrderHistory).all(methodNotAllowed);
router.route("/check-member").post(checkMember).all(methodNotAllowed);
router
  .route("/product-favorite")
  .get(getProductFavorite)
  .post(saveProductFavorite)
  .all(methodNotAllowed);
router.route("/register").post(register).all(methodNotAllowed);
router.route("/signout").post(signOut).all(methodNotAllowed);
router.route("/membership/info").post(getMembershipInfo).all(methodNotAllowed);
router.route("/notification").post(saveNotification).all(methodNotAllowed);
router.route("/change-email").post(changeMemberEmail).all(methodNotAllowed);
router.route("/multi-address").get(getMultiAddress).post(addMultiAddress).delete(deleteMultiAdress).all(methodNotAllowed);
router.route("/benefit").post(getBenefit).all(methodNotAllowed);
router.route("/terms-and-conditions").post(getTermsAndConditions).all(methodNotAllowed);
router.route("/profile-image").put(updateProfileImage).all(methodNotAllowed);
router.route("/remove").post(removeCustomer).all(methodNotAllowed);
